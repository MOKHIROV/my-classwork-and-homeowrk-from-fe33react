import React from 'react';
import styles from './HeaderUser.module.scss';
import {useSelector} from "react-redux";

const HeaderUser = () => {
    const user = useSelector((state)=> state.user.user)
    console.log(user)
    if(!user){
        return null
    }
    return (

        <div className={styles.container}>
            <img
                src={user.avatar}
                alt={user.name}
                width={30}
                height={30}
            />
            <span>{user.name}</span>
        </div>
    )
}

export default HeaderUser;