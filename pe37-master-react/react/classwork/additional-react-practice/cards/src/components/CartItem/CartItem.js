import React from 'react';
import PropTypes from 'prop-types';
import styles from './CartItem.module.scss';
import Button from '../Button'
import {ReactComponent as CartIcon} from "../../svg/delete.svg";
import {useDispatch} from "react-redux";
import {setConfigModal, setIsOpenModal} from "../../store/actionCreators/modalAC";


const CartItem = (props) => {
const dispatch = useDispatch()
    const {count, title, id, img} = props
const handleDelete = () =>{
    dispatch(setIsOpenModal(true))
    dispatch(setConfigModal({title, id}))
}
    return (
        <>

        <div className={styles.cartItem}>
            <div className={styles.contentContainer}>
                <div className={styles.imgWrapper}>
                    <img className={styles.itemAvatar} src={img} alt={title}/>
                </div>
                <span className={styles.title}>{title}</span>
            </div>


            <span className={styles.quantity}>{count}</span>

            <div className={styles.btnContainer}>
                <Button className={styles.btn}>+</Button>
                <Button className={styles.btn}>-</Button>
                <Button onClick={handleDelete} className={styles.btn} color="red"><CartIcon/></Button>
            </div>

        </div>
            </>
    )
}

CartItem.propTypes = {};
CartItem.defaultProps = {};

export default CartItem;