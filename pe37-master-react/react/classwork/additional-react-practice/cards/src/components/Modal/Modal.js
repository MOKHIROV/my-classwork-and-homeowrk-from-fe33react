import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button'
import {useDispatch, useSelector} from "react-redux";
import {setIsOpenModal} from "../../store/actionCreators/modalAC";
import {deleteCartItem} from "../../store/actionCreators/cartAC";

const Modal= () => {
    const isOpen = useSelector(state => state.modal.isOpen)
    const config = useSelector(state => state.modal.config)
    console.log(config)
const dispatch = useDispatch()
    if (!isOpen) return null;

    const closeModal = ()=>{
        dispatch(setIsOpenModal(false))
    }
    const handleYes = () =>{
        dispatch(deleteCartItem(config.id))
        dispatch(setIsOpenModal(false))
    }
    console.log(config.id)
    return (
         <div className={styles.root}>
             <div onClick={closeModal} className={styles.background} />
             <div className={styles.content}>
                 <div className={styles.closeWrapper}>
                     <Button onClick={closeModal} className={styles.btn} color="red">X</Button>
                 </div>
                 {/*<h2>Do you really want to delete {config && config.title} ?</h2>*/}
                 <h2>Do you really want to delete {config?.title} ?</h2>
                 <div className={styles.buttonContainer}>
                     <Button onClick={handleYes}>Yes</Button>
                     <Button onClick={closeModal} color="red">No</Button>
                 </div>
             </div>
         </div>
    );
};

export default Modal;
