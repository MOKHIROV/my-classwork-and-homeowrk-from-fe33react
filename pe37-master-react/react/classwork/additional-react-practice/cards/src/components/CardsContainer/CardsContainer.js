import React from "react"
import CardItem from "../CardItem";
import PropTypes from "prop-types";
import Preloader from "../Preloader";
import {useSelector} from "react-redux";
import styles from "./CardsContainer.module.scss"

const CardsContainer = (props) => {
    const isLoading = useSelector(state => state.cards.isLoading)
    console.log(isLoading)
    const {data} = props
    return (
        <section className={styles.cardsContainer}>
            {isLoading && <Preloader size={100} borderWidth={10}/>}
            {!isLoading && data.map((elem)=><CardItem title={elem.title} description={elem.description} id={elem.id} img={elem.img} key={elem.id}/>)}
        </section>
    )
}

CardsContainer.propTypes = {
    data: PropTypes.array
}

CardsContainer.defaultProps = {
    data: []
}


export  default  CardsContainer;