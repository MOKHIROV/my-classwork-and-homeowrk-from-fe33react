import React from 'react';
import PropTypes from 'prop-types';
import styles from './CardItem.module.scss';
import Button from '../Button';
import {useDispatch} from "react-redux";
import {addCartItemAC} from "../../store/actionCreators/cartAC";

const CardItem = (props) => {
    const {title,img,description,id} = props
    const dispatch = useDispatch()

    return (
        <div className={styles.card}>
            <button type="button" className={styles.likeButton}>Like</button>
            <span className={styles.title}>{title}</span>
            <img className={styles.itemAvatar} src={img} alt={description}/>
            <span className={styles.description}>{description}</span>

            <div className={styles.btnContainer}>
                <Button onClick={()=> dispatch(addCartItemAC({title, img, id}))}>ADD TO CART</Button>
            </div>
        </div>
    )
}

CardItem.propTypes = {};
CardItem.defaultProps = {};

export default CardItem;