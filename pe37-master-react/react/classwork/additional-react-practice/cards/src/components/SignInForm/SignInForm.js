import React, {useState} from "react";
import {Formik, Form, Field, ErrorMessage} from 'formik';
import * as yup from 'yup';
import styles from "./SignInForm.module.scss"
import axios from "axios";
import {useDispatch, useSelector} from "react-redux";
import {addDataUserAC} from "../../store/actionCreators/userAC";
import {useNavigate} from "react-router";
import Button from "../Button";
import Preloader from "../Preloader";

const SignInForm = () => {
    const [isLoading, setIsLoading] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const initialValues = {
        email: "",
        password: "",
    }
    const schema = yup.object().shape({
        email: yup.string().email(),
    })


    const onSubmit = async (values) => {
        try {

                const data = await fetch('http://localhost:3001/sign-in',
                    {
                        method:'POST',
                        body: JSON.stringify(values),
                        headers:{
                            'content-type':'application/json',
                        }
                    }).then(res => res.json())
            setIsLoading(true)
            // const {data: {data}} = await axios.post('http://localhost:3001/sign-in', values);
            dispatch(addDataUserAC(data.user, data.token))
            navigate("/")
            setIsLoading(false)
        } catch (error) {
            setIsLoading(false)
        }

    }

    return (

        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={schema}>
            <Form className={styles.form}>
                <Field className={styles.input} name='email' placeholder='Your email'/>
                <ErrorMessage name="email" render={msg => <span className={styles.error}>{msg}</span>}/>
                <Field className={styles.input} name='password' placeholder='Your password'/>

                {!isLoading ? <Button>Submit</Button> : <Preloader/>}

            </Form>

        </Formik>
    )
}
export default SignInForm;