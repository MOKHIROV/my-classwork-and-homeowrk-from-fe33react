import React from "react"
import PropTypes from "prop-types";
// import Preloader from "../Preloader";
import {useSelector} from "react-redux";
import styles from "./CartContainer.module.scss"
import CartItem from "../CartItem";

const CartContainer = () => {
    const cartData = useSelector((state)=>state.cart.cards)
    console.log(cartData)

    return (
        <section className={styles.cartContainer}>
            {cartData.map((elem)=><CartItem count={elem.count} title={elem.title} id={elem.id} img={elem.img} key={elem.id}/>)}
        </section>
    )
}

export  default  CartContainer;