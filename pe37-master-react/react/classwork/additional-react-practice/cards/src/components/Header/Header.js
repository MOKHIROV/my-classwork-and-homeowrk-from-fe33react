import React from 'react';
import styles from './Header.module.scss';
import {Link} from "react-router-dom";
import {ReactComponent as SignInSVG} from "../../svg/account-arrow-left.svg";
import {ReactComponent as SignOutSVG} from "../../svg/account-arrow-right.svg";
import HeaderUser from "../HeaderUser";
import {useDispatch, useSelector} from "react-redux";
import {removeDataUserAC} from "../../store/actionCreators/userAC";
import {removeCardsAC} from "../../store/actionCreators/cardsAC";

const Header = () => {
    const user = useSelector((state)=> state.user.user);
    const dispatch = useDispatch()

    return (
        <header className={styles.root}>
            {/* Left container */}
            <div>
                <HeaderUser />
            </div>

            {/* Centered navigation */}
            <nav>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>

                </ul>
            </nav>

            {/* Right container */}
            <ul>
                <li>
                    <Link to="/cart">Cart</Link>
                </li>
                {!user && <li>
                    <Link to="/sign-in"><SignInSVG/></Link>
                </li>}


                { user && <li>
                    <a onClick={() => {
                        dispatch(removeDataUserAC());
                        dispatch(removeCardsAC())
                    }}><SignOutSVG/></a>
                </li>}

            </ul>
        </header>
    );
};

export default Header;
