import {GET_DATA, REMOVE_DATA, SET_IS_LOADING_CARDS} from "../actions/cardsActions";

export  const  getDataAC = (token) => async (dispatch) =>{
    dispatch(setIsLoading(true));
    const {data} = await fetch('http://localhost:3001/items',{
        method:"GET",
        headers:{
            "Authorization":`Bearer ${token}`
        }
    })
        .then(rsp => rsp.json())
    dispatch(setIsLoading(false));
    dispatch({type:GET_DATA,payload:data})
}

export const setIsLoading = (value) => ({type: SET_IS_LOADING_CARDS, payload: value})

export const removeCardsAC = () => ({type: REMOVE_DATA})