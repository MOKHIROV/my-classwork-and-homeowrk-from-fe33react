import {ADD_DATA, REMOVE_DATA} from "../actions/userActions";

export const addDataUserAC = (user, token)=> {
    return {type: ADD_DATA, payload: {user, token}}
}
export const removeDataUserAC = ()=> {
    return {type: REMOVE_DATA}
}
