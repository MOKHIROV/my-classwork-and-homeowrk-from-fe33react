import {ADD_CART_ITEM, DELETE_CART_ITEM} from "../actions/cartActions";

export const addCartItemAC = (data)=> {
    return {type: ADD_CART_ITEM, payload: data}
}
export const deleteCartItem = (id) =>{
    return {type: DELETE_CART_ITEM, payload: id}
}
// export const removeDataUserAC = ()=> {
//     return {type: REMOVE_DATA}
// }