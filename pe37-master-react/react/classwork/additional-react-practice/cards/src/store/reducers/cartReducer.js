import {ADD_CART_ITEM, DELETE_CART_ITEM} from "../actions/cartActions";
import {getItemFromLS, setItemToLS} from "../../utils/localStorage";
getItemFromLS('cart')

const initialValues ={
    cards: getItemFromLS('cart') ? getItemFromLS('cart').cards : [],
}


const cartReducer = (state = initialValues, action) => {
    switch (action.type){
        case ADD_CART_ITEM: {
            const {title, img, id} = action.payload;

            const index = state.cards.findIndex((item) => item.id === action.payload.id)
            console.log(index)
            if(index === -1) {
                const newState = {...state, cards: [...state.cards, {title, img, id, count: 1}] }

                setItemToLS('cart', newState)
                return newState;
            }

            const newCartData = [...state.cards];
            newCartData[index].count += 1

            setItemToLS('cart', {...state, cards: newCartData})
            return {...state, cards: newCartData}
        }
        case DELETE_CART_ITEM:{
            const index = state.cards.findIndex((item) => item.id === action.payload)
            console.log(index)
            if(index === -1){
                return state
            }
            const newCartData = [...state.cards];
            newCartData.splice(index, 1)

            setItemToLS('cart', {...state, cards: newCartData});
            return {...state, cards: newCartData}
        }

        default: return state
    }

}

export default cartReducer