import {combineReducers} from "redux";
import userReducer from "./userReducer";
import cardsReducer from "./cardsReducer";
import cartReducer from "./cartReducer";
import modalReducer from "./modalReducer";

const appReducer = combineReducers({
    user: userReducer,
    cards:cardsReducer,
    cart: cartReducer,
    modal: modalReducer,
})

export default appReducer