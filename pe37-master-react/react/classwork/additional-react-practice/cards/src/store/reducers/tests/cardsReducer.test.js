import cardsReducer from "../cardsReducer";
import {GET_DATA, REMOVE_DATA, SET_IS_LOADING_CARDS} from "../../actions/cardsActions";

const initialValues ={
    cards:[],
    isLoading:false,
}


describe("Cards Reducer works", ()=>{
    test("Should cardReducer returns state if params null",()=>{
        expect(cardsReducer()).toEqual(initialValues)
    })

})

describe("Check actions work", ()=> {
    test("Should action GET_DATA works", ()=> {
        expect(cardsReducer(initialValues, {type: GET_DATA, payload: "Some Value"})).toEqual({
            ...initialValues,
            cards: "Some Value"
        })
    })
    test("Should action REMOVE_DATA works", ()=> {
        const initialValuesRemove = {
            cards:[1,2],
            isLoading:false,
        }
        expect(cardsReducer(initialValuesRemove, {type: REMOVE_DATA})).toEqual({
            ...initialValuesRemove,
            cards: []
        })
    })
    test("Should action SET_IS_LOADING_CARDS works", ()=> {
        expect(cardsReducer(initialValues, {type: SET_IS_LOADING_CARDS, payload: true})).toEqual({
            ...initialValues,
            isLoading: true
        })
    })
})