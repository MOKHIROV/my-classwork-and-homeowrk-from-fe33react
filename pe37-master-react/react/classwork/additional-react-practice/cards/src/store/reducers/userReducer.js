import {ADD_DATA, REMOVE_DATA} from "../actions/userActions";

const initialValues = {
    user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null,
    token: localStorage.getItem('token') ? JSON.parse(localStorage.getItem('token')) : null,
}

const userReducer = (state = initialValues, action) => {
    switch (action.type){
        case ADD_DATA: {
            localStorage.setItem('user', JSON.stringify(action.payload.user));
            localStorage.setItem('token', JSON.stringify(action.payload.token));
            return {...state, user: action.payload.user, token: action.payload.token}
        }
        case REMOVE_DATA: {
            localStorage.removeItem('user');
            localStorage.removeItem('token');
            return {...state, user: null, token: null}
        }
        default: return state
    }
}

export default userReducer