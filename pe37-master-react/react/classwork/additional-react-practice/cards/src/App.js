import './App.scss';
import AppRoutes from "./Routes/Routes";
import Header from "./components/Header";
import Modal from "./components/Modal";
import {useEffect} from "react";
import {getDataAC} from "./store/actionCreators/cardsAC";
import {useDispatch, useSelector} from "react-redux";

function App() {
    const token = useSelector((state)=> state.user.token);
    const dispatch = useDispatch()

    useEffect(()=>{
        if(token){
            dispatch(getDataAC(token))
        }
    },[token])

    return (
        <>
            <Header/>
            <section>
                <Modal/>
                <AppRoutes/>
            </section>
        </>
    );
}

export default App;
