import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getDataAC} from "../../store/actionCreators/cardsAC";
import {Link} from "react-router-dom";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import CartItem from "../../components/CartItem";
import Modal from "../../components/Modal";

const HomePage = () => {
    const data = useSelector((state)=> state.cards.cards)
    const isLoading = useSelector(state => state.cards.isLoading)


    return (
        <>
            <h1 >Home page</h1>

            {!data.length && !isLoading && <p>Авторизируйтесь <Link to="/sign-in">здесь</Link></p>}
            {data && <CardsContainer data={data}/>}

        </>
    )
}
export default HomePage;