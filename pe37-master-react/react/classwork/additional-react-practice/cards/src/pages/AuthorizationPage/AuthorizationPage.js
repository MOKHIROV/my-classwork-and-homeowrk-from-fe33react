import React from 'react';
import SignInForm from "../../components/SignInForm/SignInForm";

const AuthorizationPage = () => {

    return (
        <>
            <h1>Sign-in page</h1>
            <SignInForm/>
        </>
    )
}
export default AuthorizationPage;