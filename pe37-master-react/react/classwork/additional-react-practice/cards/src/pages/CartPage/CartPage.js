import React from 'react';
import CartContainer from "../../components/CartContainer/CartContainer";

const CartPage = () => {

    return (
        <>
            <h1>Cart</h1>
            <CartContainer/>
        </>
    )
}
export default CartPage;