import React from 'react';
import PropTypes from 'prop-types';
import {Routes, Route} from "react-router-dom";
import HomePage from '../pages/HomePage/HomePage'
import AuthorizationPage from "../pages/AuthorizationPage/AuthorizationPage";
import CartPage from "../pages/CartPage/CartPage";

const AppRoutes = () => {
    return (
        <Routes>
            <Route path='/' element={<HomePage/>}/>
            <Route path='/sign-in' element={<AuthorizationPage/>}/>
            <Route path='/cart' element={<CartPage/>}/>
        </Routes>
    )
}
export default AppRoutes;