import React , { Component } from 'react';

class Button extends Component {
    render() {
        const {backgroundcolor ,handleClick , children } = this.props;
        return(
            <button style={{backgroundColor: backgroundcolor}} onClick={handleClick}>{children}</button>
        )
    }
}

export default Button;