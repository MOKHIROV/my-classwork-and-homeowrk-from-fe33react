import style from './Modal.module.scss'
import React , { Component} from 'react'
export default class Modal extends Component {
    state = {
        closeButton: false
    }
    render() {
        const { header , text , onClick } = this.props;
        return (
          <div className={style.basicModal} text={header} header={header} onClick={onClick}>
            <div className={style.modal} >
            <p className={style.modalX} onClick={() => onClick}>X</p>
            <p className={style.modalTitle}>{header}</p>
            <p className={style.modalFirstText}>
                {text}
            </p>
            <p className={style.modalSecondText}>
                Lorem ipsum dolor sit amet, consectet
            </p>
            <button className={style.modalFirstButton} onClick={() => onClick}>Ok</button>
            <button className={style.modalSecondButton} onClick={() => onClick}>Cancel</button>
          </div>
        </div>
              
        )
    }
}
