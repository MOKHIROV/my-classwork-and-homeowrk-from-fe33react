import React, { Component } from "react";
import Button from "./Components/Button/Button.js";
import Modal from "./Components/Modal/Modal.js";
import SecondModal from "./Components/Modal/SecondModal.js";
class App extends Component {
  state = {
    isOpenFirst: false,
    isOpenSecond: false,
  };

  openModal = (isSecond = false) => {
    if (isSecond) {
      this.setState({ isOpenSecond: true });
    } else {
      this.setState({ isOpenFirst: true });
    }
  };

  closeModal = (isSecond = false) => {
    if (isSecond) {
      this.setState({ isOpenSecond: false });
    } else {
      this.setState({ isOpenFirst: false });
    }
  };

  render() {
    const { isOpenSecond, isOpenFirst } = this.state;
    return (
      <>
        {isOpenFirst && (
          <Modal text="Basic text" header="Header" onClick={() => this.closeModal()}/>
        )}
        {isOpenSecond && (
          <SecondModal onClick={() => this.closeModal(true)} text="Big text" header="Title"/>
        )}
        <Button backgroundcolor={"red"} handleClick={() => this.openModal()}>
          Open first modal
        </Button>
        <Button
          backgroundcolor={"green"}
          handleClick={() => this.openModal(true)}
        >
          Open second modal
        </Button>
      </>
    );
  }
}

export default App;
