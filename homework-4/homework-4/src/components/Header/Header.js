import {Link, BrowserRouter } from 'react-router-dom';
import styles from '../Header/Header.module.scss';
const Header = () => {
  return (
        <nav className={styles.header}>
          <ul className={styles.ul}>
            <li className={styles.li}>
              <Link className={styles.ml} to="/">
                Home
              </Link>
            </li>
            <li>
              <Link className={styles.nav} to="/cart">
                Cart
              </Link>
            </li>
            <li>
              <Link className={styles.mr} to="/favorites">
                Favorites
              </Link>
            </li>
          </ul>
        </nav>





  );
};
export default Header;
