import style from '../renderCards/renderCards.module.scss';
import PropTypes from 'prop-types';
import React, {useState} from "react";
import CardItem from "../CardItem/CardItem";
import { useSelector} from "react-redux";

const RenderCards =()=> {
  // const { products } = useSelector((state)=> state.products)
  const [state, setState] = useState({
      product: [],
  });
  fetch('/data/sellCards.json')
      .then((res) => res.json())
      .then((data) => setState({product: data}) );
     const { product }= state;
    return (
      <>
        <div className={style.cardsStyle}>
          {product.map((p) => <CardItem key={p.id} name={p.name} devicePhoto={p.devicePhoto} price={p.price} id={p.id} colour={p.colour} /> )}
        </div>
      </>
    );
}

RenderCards.propTypes = {
  device: PropTypes.string,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  article: PropTypes.string,
  colour: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  price: PropTypes.string,
};
RenderCards.defaultProps = {
  device: '',
  name: 'name',
  article: null,
  colour: 'grey',
  price: null,
};
export default RenderCards;
