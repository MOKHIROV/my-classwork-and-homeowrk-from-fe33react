import React from "react";
import {useSelector} from "react-redux";
import CartItem from "../CartItem/CartItem";
const CartContainer = () => {
    let { cards }  = useSelector((state)=> state.cards )
    return(
        <>
        {cards.map((item)=> <CartItem key={item.id} name={item.name} devicePhoto={item.devicePhoto} price={item.price} id={item.id}/>)}
        </>
   )
}
export default CartContainer;