import React from 'react';
import style from "../renderCards/renderCards.module.scss";
import Modal from "../Modal/Modal";
import {addToCart, addToFavorites} from "../../store/creators/actions";
import { useDispatch} from "react-redux";

const CardItem = (props) => {
    const { devicePhoto, name, id, colour, price  } = props;
    const dispatch = useDispatch();
    const handleAdd = () => {
        dispatch(addToCart({name, devicePhoto, price , id}))
    }
    const handleAddToFavorites = () => {
        dispatch(addToFavorites({name , devicePhoto , price , id}))
    }
    return(
        <div className={style.border} key={id}>
            <img className={style.images} src={devicePhoto}></img>
            <h1 className={style.firstH1}>{name}</h1>
            <p className={style.secondP}>Артикль: {id}</p>
            <p className={style.secondP}>Цвет: {colour}</p>
            <p className={style.priceP}>Цена: {price}</p>
            <Modal name={name} id={id} price={price} clickSecond={handleAdd} clickToFavorite={handleAddToFavorites} devicePhoto={devicePhoto}/>
        </div>
    )
}

export default CardItem;