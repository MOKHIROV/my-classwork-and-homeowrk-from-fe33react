import React from "react";
import {useSelector} from "react-redux";
import CartItem from "../CartItem/CartItem";
import FavoritesItem from "../FavoritesItem/FavoritesItem";
const FavoritesContainer = () => {
    let { favorites }  = useSelector((state)=> state.cards )
    return(
        <>
        {favorites.map((item)=> <FavoritesItem key={item.id} name={item.name} devicePhoto={item.devicePhoto} price={item.price} id={item.id}/>)}
        </>
   )
}
export default FavoritesContainer;