import React, {Component, useState } from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
import style from './Modal.module.scss'
const Modal  =(props )=> {
  const [state , setState] = useState({
      isOpned:false,
                                      })
  const openModal = (isSecond = false) => {
    if (isSecond) {
      setState({ isOpned: true });
    }
  };

  const closeModal = (isSecond = false) => {
    if (isSecond) {
      setState({ isOpned: false });
    }
  };

    const { header, name , price , id , devicePhoto , text , clickSecond, clickToFavorite } = props;
    const { isOpned } = state;
    return (
      <>
        <Button name={name} price={price} id={id} devicePhoto={devicePhoto} clickToFavorite={clickToFavorite} handleClick={() => openModal(true)} backgroundcolor="blue">
          Add to Cart
        </Button>
        {isOpned && (
          <div className={style.basicModal} text={text} header={header}>
            <div className={style.modal}>
              <p className={style.modalX} onClick={() => closeModal(true)}>
                X
              </p>
              <p className={style.modalTitle}>{header}</p>
              <p className={style.modalSecondText}>
                Press "Yeah" if you want add this item to your cart
              </p>
              <button
                className={style.modalFirstButton}
                onClick={ clickSecond}>
                Yeah
              </button>
              <button
                className={style.modalSecondButton}
                onClick={()=> closeModal(true)}>
                No
              </button>
            </div>
          </div>
        )}
      </>
    );
}
Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
};
Modal.defaultProps = {
  header: 'Add to your cart?',
  text: 'Yes or No',
};
export default Modal;
