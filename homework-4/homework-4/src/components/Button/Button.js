import React, { useState, Component } from 'react';
import style from '../Button/Button.module.scss';
import SvgIcon from '../svg/SvgIcon';
import PropTypes from 'prop-types';
const Button = (props) => {
  const { backgroundcolor, handleClick, children, clickToFavorite } = props;
  return (
    <>
      <div className={style.buttonDiv}>
        <button
          className={style.buttons}
          style={{ backgroundColor: backgroundcolor }}
          onClick={handleClick}>
          {children}
        </button>
          <SvgIcon
              clickToFavorite={clickToFavorite}
            className={style.svg}
          />
      </div>
    </>
  );
};
Button.propTypes = {
  backgroundColor: PropTypes.string,
  handleClick: PropTypes.func,
  children: PropTypes.any.isRequired,
};
Button.defaultProps = {
  backgroundColor: 'grey',
  handleClick: () => {},
  children: '',
};
export default Button;
