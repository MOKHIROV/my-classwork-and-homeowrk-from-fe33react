import React from "react";
import styles from './CartItem.module.scss';
import { useDispatch} from "react-redux";
import { deleteCartItem} from "../../store/creators/actions";

const CartItem = (props) => {
    const { name , devicePhoto ,price , id} = props;
    const dispatch = useDispatch()
    const handleDelete = ()=> {
        dispatch(deleteCartItem({ id, action: 'delete'}))
    }
    return(
        <div className={styles.marginLeft}>
            <button onClick={handleDelete} className={styles.cancel}>X</button>
        <div className={styles.cartDiv}  key={id}>
            <img src={devicePhoto}/>
            <h1 className={styles.marginRight} >{name}</h1>
            <h2 className={styles.marginRight}>Цена: {price}</h2>
            <h3 className={styles.marginRight}>Артикул: {id}</h3>
            <button>Оформить покупку</button>
        </div>
        </div>
    )
}
export default CartItem;