import React from "react";
import styles from '../components/CartItem/CartItem.module.scss'
import FavoritesContainer from "../components/FavoritesContainer/FavoritesContainer";
const Favorite = () => {
    return(
        <>
        <h1 className={styles.pages}>Favorites</h1>
            <FavoritesContainer/>
        </>
    )
}
export default Favorite;