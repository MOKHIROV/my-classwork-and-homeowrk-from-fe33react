import React  from "react";
import RenderCards from "../components/renderCards/renderCards";
import styles from '../components/CartItem/CartItem.module.scss'
const Home = () =>{
    return(
        <>
            <h1 className={styles.pages}>Products</h1>
        <RenderCards/>
        </>
    )
}
export default Home;