import React from "react";
import CartContainer from "../components/CartContainer/CartContainer";
import {useSelector} from "react-redux";
import styles from '../components/CartItem/CartItem.module.scss'
const Cart = () => {
    const { text} = useSelector((state)=> state.cards)
    return(
        <>
        <h1 className={styles.pages}>Cart</h1>
            {!JSON.parse(localStorage.getItem('cart')) ? <h1 className={styles.noItems}>{text}</h1>: <CartContainer/>  }
            </>
    )
}
export default Cart;