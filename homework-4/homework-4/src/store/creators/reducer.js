import { actionsTypes } from './actions';
import { getItemFromLS, setItemToLS} from '../../utils/localStorage'
const initialState = {
    cards : getItemFromLS('cart') ? getItemFromLS('cart').cards : [],
    favorites: getItemFromLS('favorites') ? getItemFromLS('favorites').favorites : [],
    products: null,
    text: 'No items in your Cart!'
}
const data = fetch('/data/sellCards.json')
    .then((res) => res.json())

export const cartReducer = (state = initialState, action) => {
    switch (action.type){
        case actionsTypes.GET_PRODUCTS : {
            return {...state , products: [data]}
            console.log(state.products)
        }
        case actionsTypes.ADD_TO_CART : {
            const {name, devicePhoto, price , id} = action.payload;
            const index = state.cards.findIndex((item) => item.id === action.payload.id)
            console.log(index)
            if(index === -1) {
                const newState = {...state, cards: [...state.cards, {name, devicePhoto, price , id ,  count: 1}] }

                setItemToLS('cart', newState)
                return newState;
            }

            const newCartData = [...state.cards];
            newCartData[index].count += 1

            setItemToLS('cart', {...state, cards: newCartData})
            return {...state, cards: newCartData}
    }
        case actionsTypes.DELETE_FROM_CART :{

                const index = state.cards.findIndex((item) => item.id === action.payload.id)
                if(index === -1){
                    return state
                }
                const newCartData = [...state.cards];
                newCartData.splice(index, 1)

                setItemToLS('cart', {...state, cards: newCartData});
                return {...state, cards: newCartData}
        }
        case actionsTypes.ADD_TO_FAVORITES : {
            const {name, devicePhoto, price , id} = action.payload;

            const index = state.favorites.findIndex((item) => item.id === action.payload.id)
            console.log(index)
            if(index === -1) {
                const newState = {...state, favorites: [...state.favorites, {name, devicePhoto, price , id ,  count: 1}] }

                setItemToLS('favorites', newState)
                return newState;
            }

            const newCartData = [...state.favorites];
            newCartData[index].count += 1

            setItemToLS('favorites', {...state, favorites: newCartData})
            return {...state, favorites: newCartData}
        }
        case actionsTypes.DELETE_FROM_FAVORITES : {
            const index = state.favorites.findIndex((item) => item.id === action.payload.id)
            if(index === -1){
                return state
            }
            const newCartData = [...state.favorites];
            newCartData.splice(index, 1)

            setItemToLS('cart', {...state, favorites: newCartData});
            return {...state, favorites: newCartData}
        }


default: return  state
    }
}

