import data from "bootstrap/js/src/dom/data";

export const actionsTypes = {
    ADD_TO_CART : 'ADD_TO_CART',
    DELETE_FROM_CART : 'DELETE_FROM_CART',
    ADD_TO_FAVORITES : 'ADD_TO_FAVORITES',
    DELETE_FROM_FAVORITES: 'DELETE_FROM_FAVORITES',
    GET_PRODUCTS: 'GET_PRODUCTS'
}

export const addToCart  = (data) => {
    return {type: actionsTypes.ADD_TO_CART, payload: data}
}
export const deleteCartItem  = (id)=>{
    return  {type: actionsTypes.DELETE_FROM_CART , payload: id}
}
export const addToFavorites = (data)=> {
    return {type: actionsTypes.ADD_TO_FAVORITES, payload: data}
}
export const deleteFavoritesItem  = (id)=>{
    return  {type: actionsTypes.DELETE_FROM_FAVORITES , payload: id}
}

export const getPoducts = (data) => {
    return {type: actionsTypes.GET_PRODUCTS , payload: data}
}