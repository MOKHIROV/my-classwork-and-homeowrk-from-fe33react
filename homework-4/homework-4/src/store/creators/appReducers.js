import { cartReducer } from './reducer';
import { combineReducers} from "redux";

export const appReducers = combineReducers({
    cards: cartReducer
})

export default appReducers;