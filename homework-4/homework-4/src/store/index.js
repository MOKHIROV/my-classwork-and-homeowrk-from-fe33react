import { createStore, applyMiddleware} from "redux";
import { appReducers} from './creators/appReducers';
import thunk from "redux-thunk";
export const store = createStore(appReducers , applyMiddleware(thunk));