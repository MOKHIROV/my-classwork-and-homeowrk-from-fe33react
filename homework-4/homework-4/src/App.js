import React from 'react';
import Home from './pages/Home';
import Header from './components/Header/Header';
import Cart from './pages/Cart';
import {Provider} from "react-redux";
import Favorite from "./pages/Favorite";
import {store} from './store/index'
import { BrowserRouter, Route , Switch } from 'react-router-dom'
const App = () =>{
  return(
      <BrowserRouter>
          <Provider store={store}>
          <Header/>
          <Switch>
            <Route exact path="/">
              <Home/>
            </Route>
            <Route  exact path="/cart" >
              <Cart/>
            </Route>
            <Route exact path="/favorites" >
              <Favorite/>
            </Route>
          </Switch>
              </Provider>
      </BrowserRouter>
  )
}
export default App;