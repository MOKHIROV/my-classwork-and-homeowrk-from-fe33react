import {Switch, Route, BrowserRouter} from 'react-router-dom';
import Header from './components/Header/Header';
import Home from './pages/Home';
import Cart from './pages/Cart';
import Favorites from './pages/Favorite';
import { CartProvider, useCart } from "react-use-cart";
import React from "react";
const App=()=> {
  return(
<>
      <BrowserRouter>
          <Header/>
      <Switch>
          <CartProvider>
      <Route exact path="/">
        <Home/>
      </Route>
      <Route  exact path="/cart" >
        <Cart/>
      </Route>
      <Route exact path="/favorites" >
        <Favorites/>
      </Route>
          </CartProvider>
    </Switch>
      </BrowserRouter>

    </>

  )
}

export default App;
