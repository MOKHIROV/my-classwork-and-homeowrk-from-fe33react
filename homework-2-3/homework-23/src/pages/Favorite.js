import React  from 'react';
import {useCart } from "react-use-cart";
import style from './Cart.module.scss'
const Favorites = () =>{
    const {
        isEmpty, totalUniqueItems, items, totalItems, removeItem} = useCart();
    return(
        <>
            {isEmpty ? <h1 className={style.noItems}>You have no items in your Favorites</h1> :
                <>
                    <h1>Cart: ({totalUniqueItems})</h1>
                    <div className={style.cardsStyle}>
                        {items.map((item) => {
                            return (
                                <div className={style.border}>
                                    <img src={item.devicePhoto}/>
                                    <h1 className={style.firstH1}>{item.name}</h1>
                                    <h3 className={style.secondP}>Color: {item.colour}</h3>
                                    <h3 className={style.secondP}>Id: {item.id}</h3>
                                    <h2 className={style.priceP}>Price: {item.price}</h2>
                                    <button className={style.removeButton} onClick={()=> removeItem(item.id)}>Remove from my Favorites</button>
                                </div>
                            )
                        })
                        }
                    </div>
                </>
            }
        </>
    )
}
export default Favorites;