import style from '../renderCards/renderCards.module.scss';
import Modal from '../Modal/Modal';
import PropTypes from 'prop-types';
import React, {useState} from "react";
import { useCart } from "react-use-cart";
const RenderCards =(props)=> {
  const { devicePhoto, name, id, colour, price  } = props;
  const [state, setState] = useState({
    items: [],
  });
  const { addItem , addToFavorite} = useCart();
  fetch('/data/sellCards.json')
      .then((res) => res.json())
      .then((data) => setState({ items: data }));
  const { items } = state;
    return (
      <>
        <div className={style.cardsStyle}>
          {items.map((p) => (
              <div className={style.border} key={p.id}>
                <img className={style.images} src={p.devicePhoto}></img>
                <h1 className={style.firstH1}>{p.name}</h1>
                <p className={style.secondP}>Артикль: {p.id}</p>
                <p className={style.secondP}>Цвет: {p.colour}</p>
                <p className={style.priceP}>Цена: {p.price}</p>
                <Modal name={name} id={id} price={price} devicePhoto={devicePhoto} clickToFavorite={()=> addItem(p)} clickSecond={()=> addItem(p)}/>
              </div>
          ))}
        </div>
      </>
    );
}

RenderCards.propTypes = {
  device: PropTypes.string,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  article: PropTypes.string,
  colour: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  price: PropTypes.string,
};
RenderCards.defaultProps = {
  device: '',
  name: 'name',
  article: null,
  colour: 'grey',
  price: null,
};
export default RenderCards;
