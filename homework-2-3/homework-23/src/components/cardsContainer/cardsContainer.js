import React from "react"
import CardItem from "../src/components/cardItem/cardItem";
import PropTypes from "prop-types";

const CardsContainer = (props) => {
    const {data} = props
    return (
        <section>
            {data.map((elem)=><CardItem title={elem.title} description={elem.description} id={elem.id} img={elem.img} key={elem.id}/>)}
        </section>
    )
}

CardsContainer.propTypes = {
    data: PropTypes.array
}

CardsContainer.defaultProps = {
    data: []
}


export  default  CardsContainer;
