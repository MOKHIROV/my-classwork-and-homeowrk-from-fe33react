// # Завдання
// Написати функціонал логіну на сайт
// * при натисканні на кнопку `Login` перевіряються введені дані
// * через 1 секунду, якщо дані валідні, то виводиться модальне вікно з текстом Success
// * інакше в блок помилок на сторінці виводиться повідомлення _Invalid data_

const allowedUsers = [
    {username:'Grigoriy',password:'grigoriy1234'},
    {username:'Schweppes',password:'grigoriy1234'},
    {username:'Igor',password:'grigoriy1234'},
    {username:'Vitaliy',password:'grigoriy1234'}
]

function verifyUser(username,password){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            const foundUser = allowedUsers.find((currUser)=>{return currUser.username === username})
            if (foundUser && foundUser.password === password){
                resolve(foundUser)
            }
            reject(new Error("_Invalid data_"))
        },1000)
    })
}

const loginBTN = document.querySelector('#btn')
loginBTN.addEventListener('click', (event)=>{
    event.preventDefault()
    let password = document.querySelector('#password').value,
     username = document.querySelector('#username').value,
     errors = document.querySelector('.errors')
     errors.innerText = ''
    if (password && username){
        document.getElementById('username').value = ''
        document.getElementById('password').value = ''

        verifyUser(username,password)
        .then(()=> alert('Success!'))
        .catch((err)=>  errors.innerText = err.message)
    }
    else{
        errors.innerText = "Username and password are both required"
    }
})