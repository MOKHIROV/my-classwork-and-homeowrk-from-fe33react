import fetchPhotos from "../services/fetchPhotos.js"
import checkIfNextPageByID from "../services/checkNextPage.js"
class Pagination {
    values = []
    pageNumber = 0
    hasNextPage = true;
    hasPrevPage = false;
    constructor(prevBTNText = "Previous", nextBTNText = "Next") {
        this.prevBTNText = prevBTNText
        this.nextBTNText = nextBTNText
    }
    getTemplate() {
        return `
        <nav>
        <ul class="pagination">
          <li id="nextBtn" class="page-item"><a class="page-link" href="#">${this.prevBTNText}</a></li>
          <li id="prevBtn" class="page-item"><a class="page-link" href="#">${this.nextBTNText}</a></li>
        </ul>
      </nav>
      `
    }
    render(id) {
        const container = document.getElementById(id)
        if (!(container instanceof HTMLElement)) throw new Error('Absent DOM Element')
        container.insertAdjacentHTML("afterbegin", this.getTemplate());
    }

    async moveToPage(direction) {
        if ((direction === 'next' && this.hasNextPage) || (direction === 'prev' && this.hasPrevPage)){
            this.pageNumber = direction === 'next'
            ? this.pageNumber + 1
            : this.pageNumber - 1
            this.values = await fetchPhotos(this.pageNumber)
            
            if (typeof this.callback === 'function') this.callback(this.values)
        }
        this.updatePageStatus()
    }

    subscribesOnChange(callback) {
        this.callback = callback
    }
    async updatePageStatus() {
        const hasNextPage = await checkIfNextPageByID(this.pageNumber)
        const nextPageBtn = document.querySelectorAll('.page-item')[1]
        const prevPageBtn = document.querySelectorAll('.page-item')[0]
        if (!hasNextPage) {
            nextPageBtn.classList.add('disabled')
            this.hasNextPage = false
        } else {
            this.hasNextPage = true
            nextPageBtn.classList.remove('disabled')

        }
        if (this.pageNumber - 1 < 1) {
            prevPageBtn.classList.add('disabled')
            this.hasPrevPage = false    
        } else {
            this.hasPrevPage = true;
            prevPageBtn.classList.remove('disabled')

        }
    }
    init(){
        const prevPageBtn = document.getElementById('prevBtn')
        prevPageBtn.addEventListener('click', () => this.moveToPage('prev') )
        const nextPageBtn = document.getElementById('nextBtn')
        nextPageBtn.addEventListener('click', () => this.moveToPage('next'))
        this.updatePageStatus()
    }
}

export default Pagination