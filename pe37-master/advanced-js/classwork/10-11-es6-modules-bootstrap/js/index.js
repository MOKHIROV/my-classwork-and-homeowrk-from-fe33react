import Photo from "./components/Photo.js";
import Pagination from "./components/Pagination.js";

const component = new Photo("https://picsum.photos/id/0/5616/3744", 500, 300);
const pagination = new Pagination('<=','=>')

component.render('test')
pagination.render('test')
pagination.init();
pagination.subscribesOnChange((data)=> console.log('*',data))
pagination.moveToPage('next')
// pagination.updatePageStatus()