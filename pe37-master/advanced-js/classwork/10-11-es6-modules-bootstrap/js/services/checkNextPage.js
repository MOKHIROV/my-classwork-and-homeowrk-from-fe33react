import fetchPhotosByPageId from "./fetchPhotos.js"

const checkIfNextPageByID = async function(id){
    try {
         const nextPage = await fetchPhotosByPageId(id + 1)
         if (nextPage.length === 0){
             return false
         }
         return true
    }catch(e){
        return false
    }
}

export default checkIfNextPageByID