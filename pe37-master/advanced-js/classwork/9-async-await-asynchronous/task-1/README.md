# Завдання
Показувати контент постів при зміні їх ID та їх коментарі

## Технічні вимоги
При обранні поста показувати спочатку його контент, а потім завантажувати також коментарі до поста

## Advanced
* спочатку отримувати список усіх постів і відображати наявні id в селекті на сторінці
