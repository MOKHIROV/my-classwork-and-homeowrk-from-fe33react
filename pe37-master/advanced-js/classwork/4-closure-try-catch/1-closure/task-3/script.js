/**
 * Рахувати кількість натискань на пробіл, ентер,
 * шифт та альт клавіші
 * Відображати результат на сторінці
 *
 * Створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * (https://learn.javascript.ru/closure)
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 */

function createKeyCounter(keyName) {
    let count = 0;
    return () => ++count;
}
const enter = createKeyCounter('enter')
const shift = createKeyCounter('shift')
const ctrl = createKeyCounter('ctrl')

const enterCounterSpan = document.getElementById('enter-counter')
const shiftCounterSpan = document.getElementById('shift-counter')
const ctrlCounterSpan = document.getElementById('ctrl-counter')

document.addEventListener('keydown', function (e) {
    console.log(e);
    switch (e.key) {
        case 'Enter':
            enterCounterSpan.innerText=enter();
            break;
        case 'Shift':
            shiftCounterSpan.innerText= shift();
            break;
        case 'Control':
            ctrlCounterSpan.innerText= ctrl();
            break;
    }
})