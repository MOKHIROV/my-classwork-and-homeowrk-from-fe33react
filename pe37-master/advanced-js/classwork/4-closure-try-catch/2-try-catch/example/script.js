try{
  try {
    JSON.parse('Hello')
    throw new Error('test')
  }
  catch(e){
    console.log('catch level 1')
    if(e.message === 'test'){
      console.warn('catch test error')
    }else{
      throw new Error(e)
    }
  }

}catch(e){
  console.log('catch level 0')
  console.error(e)
}