/* ЗАДАНИЕ - 1
 * Написать функцию, которая принимает 2 аргумента - имя и возраст пользователя
 * Возвращаемое значение этой функции - объект с двумя ключами name и age, куда будут записаны значения переданных функции аргументов.
 * */
function createNewUser(name, age) {
	return { name, age }
}
// function createNewUser(name, age) {
// 	let user = { name: name, age: age }

// 	return user
// }
let result = createNewUser('andrey', 26)
console.log(result)
