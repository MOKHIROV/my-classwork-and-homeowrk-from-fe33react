// * unique array
// const unique = array => array.filter((element, id) => array.indexOf(element) === id);
// const data = [1, 2, 1, 2, 3, 4, 6, 7, 4, 5, 6, 7, 8, 9, 9, 8, 6];
// const test = [...new Set(data)];
// console.log(unique(data)); // [1, 2, 3]
function highOrderFunc() {
	// console.log(arguments);
	// return Array.from(arguments)
	// console.log(arguments)
	// console.log([...arguments])
}

// console.log(highOrderFunc(12, 45, 546, 6, 7, 78, 8, 9, 87))

// const test = (...paket) => {
//     console.log(paket);
// }

// const arr = []
// console.log('🚀 ==== > arr', arr)
// const arr1 = new Array(10).fill(0).map((item, index) => ++index + item)
// console.log('🚀 ==== > arr1', arr1)
// const arr2 = Array(222)
// console.log('🚀 ==== > arr2', arr2)

// const srsStr =
// 	'Fpsum dolor sit amet consectetur adipisicing elit. Sed tempore expedita rem provident distinctio. Ut sed beatae Lorem, rem assumenda natus voluptatibus velit quae animi modi eius voluptatum nam culpa?'

// console.log(srsStr.includes('Lorem'))
// console.log(srsStr.indexOf('Lorem'))

// console.log(srsStr.charAt('Lorem'))

// const newArr = arr.filter(item => item <= 5)
// const newArr = arr.filter(function (element, index, array) {
// 	return item <= 5
// })
// console.log('🚀 ==== > newArr', newArr)

// for (let i = 0; i < arr.length; i++) {
// 	console.log(arr[i])
// }

// const test = []
// arr.forEach((currentItem, index) => {
// 	if (index < 5) {
// 		test.push(currentItem)
// 	}
// })
// const newArr = arr.map(function (item) {
// 	console.log(item);
// })
// console.log("🚀 ==== > newArr", newArr);

// // const newArr = arr.filter(item => item < 5)
const name = params => {
	const obj = {
		age: 26,
		someMethod: function () {
			obj.age++
		},
	}
}

// const arr = ['hello', 'world', 'true', 'true']
const arr = [12, 45, 546, 6, 7, 78, 8, 9, 87]
debugger
const summ = arr.reduce((accumulator, item) => accumulator + item, {})
console.log('🚀 ==== > summ', summ)

// const booleanSome = arr.some(element => element === 'hello')

// console.log('🚀 ==== > booleanSome', booleanSome)

// const booleanEvery = arr.every(function (singleWord) {
// 	return item
// })

// console.log('🚀 ==== > booleanEvery', booleanEvery)

// (function calc(firsNum, secondNum, c) {
// 	return firsNum + secondNum
// })()

// calc(1, ,3)

const fromPairs = array =>
	array.reduce((acc, value) => {
		debugger
		if (Array.isArray(value)) {
			acc[value[0]] = value[1]
		}
		return acc
	}, {})

const data = [
	['a', 1],
	['b', 2],
]
console.log(fromPairs(data)) // { 'a': 1, 'b': 2 }
