/* TASK - 3
 * Create a function that will receive an array as a single argument.
 * return value: new array, which is the exact copy of the source one.
 * Task needs to be done using: for, map(), spread operator. It means three implementations.
 */

// function cloneArray(array) {
// 	const clonedArray = []
// 	for (let elemOfArray of array) {
// 		clonedArray.push(elemOfArray)
// 	}
// 	return clonedArray
// }

// const cloneArray = array => {
// 	const newArr = array.map(function (item) {
// 		return item
// 	})
// 	return newArr
// }

// const cloneArray = array => array.map(item => item)

// const cloneArray = array => [...array]

cloneArray([12, 45, 546, 6, 7, 78, 8, 9, 87])
