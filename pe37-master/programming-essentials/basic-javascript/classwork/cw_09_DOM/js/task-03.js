/* TASK - 3
 * Get an element with class 'remove-me' and remove it from the page.
 * Find element with class 'make-me-bigger'. Replace class 'make-me-bigger' to 'active'. Class 'active' already exists in CSS.
 * */

;(function removeElem() {
	let elem = document.querySelector('.remove-me')
	// console.log("🚀 ==== > elem", elem);
	// elem.remove();
	elem.style.display = 'none'
})()

function makeBig() {
	let elem = document.querySelector('.make-me-bigger')
	// console.log("🚀 ==== > elem", elem);
	elem.classList.add('active')
	elem.classList.remove('make-me-bigger')
	// elem.className = 'active'
}

makeBig()


