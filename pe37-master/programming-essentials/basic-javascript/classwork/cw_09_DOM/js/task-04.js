/* TASK - 4
 * There is a list of items on the user screen.
 * Create a function that will find the 'run out' items. The amount of 'run out' items is equal to 0.
 * Replace the 0 inside text content of those elements to 'run out' and change the text color to red.
 */

function changeAmount() {
	let list = document.querySelectorAll('.storage-item')

	// debugger
	list.forEach(function (item) {
		let splitString = item.textContent.split('-')
		if (splitString[1].trim() === '0') {
            let newStr = item.textContent.replace(' 0', ' run out')
			item.textContent = newStr
			item.style.color = 'red'
		}
	})
}

changeAmount()

// const li = {
//     textContent: 'water - 0'
// }

// li.textContent.replace('0', 'run out')