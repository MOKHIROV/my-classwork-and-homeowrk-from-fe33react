/**
 * Task - 02
 *
 * Create your custom modal window.
 *
 * CSS and markup are placed in html and css files of this classwork.
 *
 * The modal window should appears after click on button "Show modal".
 *
 * Modal window should close after clicking "cross" btn on the top right corner of it, or after clicking anywhere on the screen except the modal itself.
 * */

// const showModalBtn = document.createElement('button')
// showModalBtn.textContent = 'Открыть модальное окно'
// document.body.prepend(showModalBtn)

// showModalBtn.addEventListener('click', () => {
// 	const modalWrapper = document.querySelector('.modal-wrapper')
// 	modalWrapper.style.display = 'flex'

// 	modalWrapper.addEventListener('click', function (event) {
// console.log('event.target', event.target)
// console.log('event.currentTarget', event.currentTarget)
// console.log('event.target.classList.contains("modal-close") ===>> ', event.target.classList.contains('modal-close'))

// 		if (event.target === event.currentTarget || event.target.classList.contains('modal-close')) {
//             modalWrapper.style.display = 'none'
// console.log('work');
// 		}
// 	})
// })

const showModalBtn = document.createElement('button')
showModalBtn.textContent = 'Открыть модальное окно'
document.body.prepend(showModalBtn)

function handleModal(text) {
	const modal = document.createElement('div')
	modal.className = 'modal-wrapper'

	modal.insertAdjacentHTML(
		'afterbegin',
		`<div class="modal">
            <button class="modal-close">x</button>
            <p class="modal-text">${text}</p>
        </div>`,
	)

	modal.addEventListener('click', function (ev) {
		if (ev.target === ev.currentTarget || ev.target.classList.contains('modal-close')) {
			modal.remove()
		}
	})

    document.body.prepend(modal)
	// return modal
}

showModalBtn.addEventListener('click', () => {
     handleModal('Lorem ipsum dolor sit amet, consectetur adipisicing elit')
    // const result = 	 handleModal('Lorem ipsum dolor sit amet, consectetur adipisicing elit')
    
    // document.body.prepend(result)
})
