/*TASK - 1
 * Show alert with message 'This is click', after the click on the 'Click me' button.
 */

const button = document.createElement('button')
button.textContent = 'Click me'
document.body.insertAdjacentHTML('beforeend', button)



button.addEventListener('click', function () {
	alert('This is click')
})

