/* TASK - 1
 * Create a square.
 * Ask the user about the size and background color of the square.
 * Create an element in JS
 * Ask the size.
 * Ask the background color.
 * Add the styles to the element in JS
 * Place the element BEFORE the first script tag on your page
 */

// function createSquare(size = 10, color = 'red', parent) {
// 	const div = document.createElement('div')
// 	div.style.backgroundColor = color
// 	div.style.width = size + 'px'
// 	div.style.height = size + 'px'
// 	parent.before(div)
// }

function createSquare(size = 10, color = '#00ff00') {
	const div = document.createElement('div')
	div.style.backgroundColor = color
	div.style.width = size + 'px'
	div.style.height = size + 'px'
	// div.style.cssText = `
    //     width: ${size}px;
    //     height: ${size}px;
    //     background-color: ${color};
    // `
	return div
}

let colorUser = prompt('Enter the color', '#0000ff')
let sizeUser = parseInt(prompt('Enter the size of form', '10'))
let script = document.querySelector('script')
const square = createSquare(sizeUser, colorUser)

script.before(square)
// script.before(createSquare(colorUser, sizeUser))
