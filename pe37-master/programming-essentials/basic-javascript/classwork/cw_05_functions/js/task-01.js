/* ЗАДАНИЕ - 1
 * Написать функцию суммирования.
 * Принимает аргументы: первое число и второе число
 * Возвращаемое функцией значение: сумма двух аргументов
 * */

function calcSum(firstNum, secondNum) {
	let firstArgs = parseFloat(firstNum);
	let secondArgs = parseFloat(secondNum);
	if (firstArgs && secondArgs) {
		return firstArgs + secondArgsz;
	} else {
		return null;
	}
}

let result = calcSum(1, 3);
console.log('first call result', result);
let result1 = calcSum('gogi');
console.log('second call result', result1);
let result2 = calcSum(1);
console.log('third call result', result2);

// function checkValidation(firstValue, secondValue) {
// 	if (firstValue && secondValue) {
// 		return firstValue + secondValue;
// 	} else if (firstValue === 0) {
// 		return secondValue;
// 	} else {
// 		return null;
// 	}
// }
