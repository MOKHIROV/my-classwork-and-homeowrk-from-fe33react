const makeTitle = str => {
	let res = ''

	for (let i = 0; i < str.length; i++) {
		if ((i + 1) % 3 === 0) {
			res += str[i].toUpperCase()
		} else {
			res += str[i]
		}
	}

	return res
}
const makeDaysBefore = dateStr => {
	return Math.round((new Date(dateStr).getTime() - Date.now()) / (24 * 60 * 60 * 1000))
}
const makeDescription = str => str.split("типа").join(" ")
