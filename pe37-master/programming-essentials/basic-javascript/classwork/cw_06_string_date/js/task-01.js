/*TASK 1
 * Write a function customCharAt(string,index)
 *   string - source string
 *   index - the index of the particular character of the string that we need
 * Return value: the character itself, that is placed on the specific index
 * */

function customCharAt(string, index) {
	// if (string.length < index) return ''
	// return string[index]
	return string.charAt(index)
}

let result = customCharAt('world', 10)
console.log('🚀 ==== > result', result)
