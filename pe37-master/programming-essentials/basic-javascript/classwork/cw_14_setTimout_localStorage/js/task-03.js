/* TASK - 3
 * Check if the localStorage has any value connected to key 'userName'.
 * If localStorage contains value by key 'userName', show message "Hello, userName", place actual value from storage instead of userName.
 * If there is no key userName in the localStorage:
 *   - ask user to enter his name.
 *   - save it in the localStorage by the key userName
 *   - show the message Hello, userName
 * */

const localStorageUserName = localStorage.getItem('userName')

if (localStorageUserName !== null) {
	console.log(`Hello ${localStorageUserName}`)
} else {
	const userName = prompt('Enter your username')
	localStorage.setItem('userName', userName)
	console.log(`Hello ${userName}`)
}


