/* TASK 2
* Create two variables. First will contain - any number, second - any string with any word inside.
* Execute the SUM of these two variables in the console
* Execute the DEFERENCE between these two variables in the console
* Execute the result of MULTIPLYING these two variables in the console
* Explain the result of every operation by your own words
* */

let a = 2;
let b = 'qwe';

console.log(a + b); // При операции сложения строчного типа данных с числовым, происходит преобразование в строку и только после этого, выполняется операция конкатенции строк(сложение)
console.log(a - b); // При любых других математических вычеслений с строчным типом данным, мы по итогу выполнения операции будет получать NaN
console.log(a * b);