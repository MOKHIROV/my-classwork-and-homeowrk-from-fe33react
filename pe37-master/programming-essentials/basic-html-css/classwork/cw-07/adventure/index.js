function parallax(elem, distance, speed) {
	const item = document.querySelector(elem);
	item.style.transform = `translateY(${distance * speed}px)`;
}

window.addEventListener('scroll', () => {
	parallax('.cool', window.scrollY, -1);
});
