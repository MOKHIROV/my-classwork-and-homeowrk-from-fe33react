import {Redirect, Route, Switch} from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import BlogPage from "./pages/BlogPage/BlogPage";

const Routes = () => (
    <Switch>
        <Route exact path="/">
            <Redirect to="/home"/>
        </Route>

        <Route exact path='/home'>
            <HomePage />
        </Route>

        <Route exact path='/blog'>
            <BlogPage />
        </Route>


        {/*<Route path="/users/:id/:test/:color" component={Test} />*/}

        {/*<Route path="/contact" render={(renderProps) => {*/}
        {/*    console.log('renderProps', renderProps);*/}
        {/*    return <Test additionalProps="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" {...renderProps} />*/}
        {/*}}/>*/}
    </Switch>
);

export default Routes;