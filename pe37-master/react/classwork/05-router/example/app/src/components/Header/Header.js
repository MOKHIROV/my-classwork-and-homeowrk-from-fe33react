import React from 'react';
import PropTypes from 'prop-types';
import styles from './Header.module.css';
import {NavLink} from "react-router-dom";

const Header = (props) => {
    const {} = props;

    return (
        <header className={styles.header}>
            <nav>
                <ul>
                    <li>
                        <NavLink exact activeClassName="active" to="/">Home</NavLink>
                    </li>
                    <li>
                        <NavLink exact activeClassName="active" to="/blog">Blog</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    )
}

Header.propTypes = {};
Header.defaultProps = {};

export default Header;