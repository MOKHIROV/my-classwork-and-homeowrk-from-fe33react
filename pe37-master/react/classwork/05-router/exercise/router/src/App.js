import {Component, useState} from 'react';
import Header from "./components/Header";
import Footer from "./components/Footer";
import './App.scss';
import {BrowserRouter as Router} from "react-router-dom";
import Routes from './Routes/Routes'
import BackButton from "./components/BackButton/BackButton";
import ScrollToTop from "./components/ScrollToTop/ScrollToTop";

const App = () => {
    const [isAuth, setIsAuth] = useState(false);

    return (
        <Router>
            <div className="App">
                <Header title={123} user={{name: 'Sam', company: 'Some Company'}}/>
                <BackButton/>
                <ScrollToTop/>
                <Routes setIsAuth={setIsAuth} isAuth={isAuth}/>
                <Footer title="PropTypes" year={new Date().getFullYear()}
                        onOrderFunc={() => console.log('Order call')}/>
            </div>
        </Router>
    );
}

export default App;
