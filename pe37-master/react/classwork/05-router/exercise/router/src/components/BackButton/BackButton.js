import React from "react";
import Button from "../Button";
import {ReactComponent as ArrowSVG} from "../../assets/arrow-left.svg";
import { useHistory } from "react-router-dom";
import styles from './BackButton.module.scss'

const BackButton = ()=>{
    const history = useHistory()

    return(
      <Button addClasses={styles.button} onClick={() => history.goBack()}><ArrowSVG/></Button>
    )
}
export default BackButton