import React from 'react';
import PropTypes from "prop-types";
import styles from './PostsContainer.module.scss';
import {Link, useHistory } from "react-router-dom";

const Post = (props) => {
        const { userId, title, body, id } = props;
        const history = useHistory();

        return (
            <Link to={`/posts/${id}`}>
            <div className={styles.root}>
                <span>User: {userId}</span>
                <h3>{title}</h3>
                <p>{body}</p>
            </div>

                <button onClick={history.goBack}>GO BACK</button>
        </Link>
        );
};

Post.propTypes = {
        userId: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
        ]),
        title: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
}

Post.defaultProps = {
        userId: 1,
}

export default Post;
