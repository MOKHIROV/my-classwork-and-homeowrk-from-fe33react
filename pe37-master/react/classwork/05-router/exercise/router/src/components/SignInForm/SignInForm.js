import React, {useState} from "react";
import Button from "../Button";
import {useHistory} from "react-router-dom";

import styles from './SignInForm.module.scss'

const SignInForm = ({ setIsAuth }) => {

    const user = {
        name: 'test',
        password: '123',
    }

    const history = useHistory()

    const [valueName,setValueName]  = useState('')
    const [valuePassword,setValuePassword]  = useState('')
    const [textError, setTextError] = useState('')

    console.log('valueName', valueName);
    console.log('valuePassword', valuePassword);

    return (
        <form onSubmit={(e) => {
            e.preventDefault()
            if(valueName === user.name && valuePassword === user.password){
                localStorage.setItem('isLogin', 'true')
                setIsAuth(true)
                history.push('/')
            } else {
                setTextError('Incorrect value')
            }
        }}>
            <input
                type="text"
                name="name"
                placeholder="Name"
                value={valueName}
                onChange={(event) => setValueName(event.target.value)}
            />
            <input
                type="text"
                name="password"
                placeholder="Password"
                value={valuePassword}
                onChange={(event) => setValuePassword(event.target.value)}
            />
            <p style={{color: 'red'}}>{textError}</p>
            <Button type="submit" >Log In</Button>
        </form>
    )
}

export default SignInForm;