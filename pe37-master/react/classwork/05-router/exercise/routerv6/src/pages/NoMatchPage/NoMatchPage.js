import React from 'react';
import PropTypes from 'prop-types';
// import styles from './NoMatchPage.module.scss'
import page404 from '../../assets/Page404.png'
import Button from '../../components/Button/Button'
import {useNavigate} from "react-router-dom";


const NoMatchPage = (props) => {
    const {} = props;
    const navigate = useNavigate()
    const goHome = ()=> {
        navigate("/");
    }
    return (
        <>
           <h1>Сторінка не знайдена</h1>
            <img src={page404} alt="page not found 404"/>
            <Button onClick={goHome}>Go Home</Button>
        </>
    )
}

NoMatchPage.propTypes = {};
NoMatchPage.defaultProps = {};

export default NoMatchPage;