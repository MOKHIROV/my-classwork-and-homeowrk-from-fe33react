import React from 'react';
import SignInForm from "../../components/SignInForm";

const SignIn = (props) => {
    const { setIsAuth } = props;



    return (
        <section>
            <h1>Sign in</h1>
            <SignInForm setIsAuth={setIsAuth} />
        </section>

    )
}

SignIn.propTypes = {};
SignIn.defaultProps = {};

export default SignIn;