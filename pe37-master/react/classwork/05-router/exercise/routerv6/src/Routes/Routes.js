import React, {useEffect, useState} from 'react';
import {Routes, Route, Navigate, useLocation} from 'react-router-dom';
import HomePage from "../pages/HomePage";
import PostsPage from "../pages/PostsPage/PostsPage";
import PostPage from "../pages/PostPage/PostPage";
import NoMatchPage from "../pages/NoMatchPage/NoMatchPage";
import SignIn from "../pages/SignIn/SignIn";

const AppRoutes = ({ setIsAuth, isAuth }) => {
    // const { pathname } = useLocation();
    // const [isLogin ,setIsLogin] = useState(localStorage.getItem('isLogin'))
    //
    // useEffect(() => {
    //     setIsLogin(localStorage.getItem('isLogin'));
    // }, [pathname])


    // console.log('pathname', pathname)
    // console.log('isLogin', isLogin)

    return (
        <Routes>
            {/*<Route exact path='/'>*/}
            {/*    <HomePage/>*/}
            {/*    {isAuth ? <HomePage/> : <Redirect to="/sign-in"/> }*/}
            {/*</Route>*/}
            {/*<Route exact path='/posts'>*/}
            {/*    <PostsPage/>*/}
            {/*</Route>*/}
            {/*<Route exact path='/authors'>*/}
            {/*    <h1>Authors heading</h1>*/}
            {/*</Route>*/}
            {/*<Route exact path='/sign-in'>*/}
            {/*    <SignIn setIsAuth={setIsAuth}/>*/}
            {/*</Route>*/}
            {/*<Route exact path='/posts/:id' component={PostPage}/>*/}
            {/*<Route path="*">*/}
            {/*    <NoMatchPage/>*/}
            {/*</Route>*/}

            <Route path='/' element={isAuth ? <HomePage/> : <Navigate to="/sign-in"/>} />
            <Route path='/posts' element={<PostsPage setIsAuth={setIsAuth} />} />
            <Route path='/sign-in' element={<SignIn />} />
            <Route path='*' element={<NoMatchPage />} />
        </Routes>
    )
}
export default AppRoutes;