import React from 'react';
import PropTypes from "prop-types";
import styles from './PostsContainer.module.scss';
import {Link, useHistory, useNavigate} from "react-router-dom";

const Post = (props) => {
        const { userId, title, body, id } = props;
        const navigate = useNavigate()

        return (
            <Link to={`/posts/${id}`}>
            <div className={styles.root}>
                <span>User: {userId}</span>
                <h3>{title}</h3>
                <p>{body}</p>
            </div>

                <button onClick={navigate(-1)}>GO BACK</button>
        </Link>
        );
};

Post.propTypes = {
        userId: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.number,
        ]),
        title: PropTypes.string.isRequired,
        body: PropTypes.string.isRequired,
}

Post.defaultProps = {
        userId: 1,
}

export default Post;
