import React from "react";
import Button from "../Button";
import {ReactComponent as ArrowSVG} from "../../assets/arrow-left.svg";
import { useNavigate } from "react-router-dom";
import styles from './BackButton.module.scss'

const BackButton = ()=>{
const navigate = useNavigate();

    return(
      <Button addClasses={styles.button} onClick={() => navigate(-1)}><ArrowSVG/></Button>
    )
}
export default BackButton