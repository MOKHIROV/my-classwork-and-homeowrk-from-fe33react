import React, {Component} from 'react'
import './App.scss'
import Header from './components/Header/Header'
import Mail from './components/Mail/Mail'

const emails = [
    {
        id: Math.random(),
        from: 'The Postman Team',
        topic: 'Announcing Postman’s New Plans and Pricing',
        body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.'
    },
    {
        id: Math.random(),
        from: 'Djinni',
        topic: 'JavaScript вакансії за вашим профілем',
        body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.'
    },
    {
        id: Math.random(),
        from: 'Natalia Pemchyshyn',
        topic: 'JavaScript Developer at GlobalNogic',
        body: 'Привіт!Я рекрутер компанії GlobalNogic. Ми зараз у пошуках Lead/Senior React Developer (фултайм/ремоут) на фармацевтичний проект. Розробляємо девайс, який моніторить стан здоров\'я свійських тварин.Стек: React, Ionic, HTML, CSS, RxJs, Java. Поспілкуємось?'
    }
]

class App extends Component {

    state = {
        title: 'Components and Props',
        user: {
            avatar: 'https://i.pravatar.cc/80',
            name: 'John',
        },
        emails: [
            {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            }, {
                id: Math.random(),
                from: 'The Postman Team',
                topic: 'Announcing Postman’s New Plans and Pricing',
                body: 'We are continuously adding capabilities to our platform—based on what you, our customers, need—serving everyone from individual developers to large enterprises with thousands and thousands of developers.We love developers and we’ve added great tools and functionality over the last 18 months including the Postman v9 update in September, API Builder, public workspaces, and much more.Over the years, enterprises have also become key customers. Postman recognizes that enterprises have significant needs beyond those of smaller teams of developers, and we’ve made massive investments this year to address those needs, too.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Djinni',
                topic: 'JavaScript вакансії за вашим профілем',
                body: 'Джин підібрав декілька вакансій за вашим профілем: JavaScript, $4500+, Київ, 3,5 роки досвіду, вище середьної англійська. Middle/Senior Frontend (React.js) $4500-5000Integrate interaction with blockchain into React frontend app to support functionality of several smart contracts. This includes, submitting transactions, updating user’s balances and other values, updating the state of submitted transactions in the state management tool and more. Chepela Valeriia, IT Recruiter at Argument.',
                isRead: false,
            },
            {
                id: Math.random(),
                from: 'Natalia Pemchyshyn',
                topic: 'JavaScript Developer at GlobalNogic',
                isRead: false,
                body: 'Привіт!Я рекрутер компанії GlobalNogic. Ми зараз у пошуках Lead/Senior React Developer (фултайм/ремоут) на фармацевтичний проект. Розробляємо девайс, який моніторить стан здоров\'я свійських тварин.Стек: React, Ionic, HTML, CSS, RxJs, Java. Поспілкуємось?'
            }
        ]

    }

    readMail = (id) => {
        const index = this.state.emails.findIndex(item => item.id === id);
        console.log(index)

        this.setState(current => {
            const newEmails = [...current.emails];
            newEmails[index].isRead = true;

            return ({ emails: newEmails});
        })
    }

    render() {
        const {title, user, emails} = this.state
        const actions = <>
            <button onClick={() => alert('sdfsdfsdfsdf')}>FROM PROPS</button>
        </>
        return (
            <>

                <Header actions={actions} title={title} user={user}/>
                <main>
                    <div className='emails'>
                        <h1>EMAILS</h1>

                        {emails.map((item) => <Mail readMail={this.readMail} key={item.id} id={item.id} from={item.from} topic={item.topic}
                                                    body={item.body} isRead={item.isRead}/>)}


                    </div>

                </main>
                <p style={{ color: 'red', backgroundColor: 'yellow' }}>skjdhfkjshkfhksjhdfkjshkdjfsd</p>

                <footer>
                    <span className='footer__title'>{this.state.title}</span>
                    <span className='footer__copyright'>{new Date().getFullYear()}, FE-33</span>
                </footer>
            </>
        )
    }
}

export default App
