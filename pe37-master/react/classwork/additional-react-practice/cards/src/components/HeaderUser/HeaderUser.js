import React from 'react';
import styles from './HeaderUser.module.scss';

const HeaderUser = () => {
    return (
        <div className={styles.container}>
            <img
                src="https://i1.sndcdn.com/avatars-rWyGLG8J4Dz4V32P-C0UwLQ-t500x500.jpg"
                alt="John Smith"
                width={30}
                height={30}
            />
            <span>John Smith</span>
        </div>
    )
}

export default HeaderUser;