import React, {useState} from 'react';
import PropTypes from 'prop-types';
import styles from './Button.module.scss';

const Button = (props) => {
    const {children, color, onClick, className} = props;
    const [isPressed, setIsPressed] = useState(false)
    const pressedStyle = isPressed ? styles.pressed : '';

    const colorsMap = {
        blue: styles.blue,
        red: styles.red,
    }

    return (
        <button
            onMouseDown={() => setIsPressed(true)}
            onMouseUp={() => setIsPressed(false)}
            onClick={onClick}
            className={`${styles.btn} ${pressedStyle} ${colorsMap[color]} ${className}`}
        >
            {children}
        </button>
    )
}

Button.propTypes = {
    children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
    onClick: PropTypes.func,
    color: PropTypes.oneOf(['blue', 'red']),
    className: PropTypes.string,
};

Button.defaultProps = {
    onClick: () => {
    },
    color: 'blue',
    className: '',
};

export default Button;