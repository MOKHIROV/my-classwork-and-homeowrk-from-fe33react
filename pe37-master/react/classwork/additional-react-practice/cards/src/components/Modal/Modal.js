import React from 'react';
import styles from './Modal.module.scss';
import Button from '../Button'

const Modal= () => {
    const isOpen = true;

    if (!isOpen) return null;

    return (
         <div className={styles.root}>
             <div className={styles.background} />
             <div className={styles.content}>
                 <div className={styles.closeWrapper}>
                     <Button className={styles.btn} color="red">X</Button>
                 </div>
                 <h2>Do you really want to delete?</h2>
                 <div className={styles.buttonContainer}>
                     <Button>Yes</Button>
                     <Button color="red">No</Button>
                 </div>
             </div>
         </div>
    );
};

export default Modal;
