import React from 'react';
import {useField} from "formik";
import './CustomInput.scss';

const CustomInput = (props) => {
    const {type, placeholder} = props;

    const [field, meta, helpers] = useField(props);
    const {error, touched} = meta
    return (
        <>
            <input
                {...field}
                type={type}
                placeholder={placeholder}

            />
            <p className='error' >{touched && error}</p>
        </>
    )
}

CustomInput.propTypes = {};
CustomInput.defaultProps = {};

export default CustomInput;