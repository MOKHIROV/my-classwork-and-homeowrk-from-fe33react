import React from "react";

function HomePage() {

    return (
        <>
            <h1>Home</h1>
            <p style={{ fontSize: 24 }}>Welcome home, {JSON.parse(localStorage.getItem('user')).name}</p>
        </>
    )
}

export default HomePage;