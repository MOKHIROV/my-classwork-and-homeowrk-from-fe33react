import React from "react";
import {Formik, Form} from "formik";
import CustomInput from "../../components/CustomInput/CustomInput";
import * as yup from 'yup';
import {useHistory} from "react-router-dom";


function SignUpPage() {
    const history = useHistory();

    const initialValues = {
        name: '',
        age: '',
        city: '',
        email: '',
        password: '',
        repeatPassword: ''
    }

    const onSubmit = async(values, {resetForm}) => {

        if(values.password !== values.repeatPassword){
            alert("Password")
            return;
        }


        const {data , status} = await fetch('http://localhost:3001/sign-up', {
            method: "POST",
            body: JSON.stringify(values),
            headers:{
                'Content-type': 'application/json',
            }
        })
            .then((response)=> response.json())

            if(status === "success"){
                resetForm();
                localStorage.setItem('user', JSON.stringify(data.user))
                localStorage.setItem('token', JSON.stringify(data.token))
                history.push('/')
            }

    }

    const validationSchema = yup.object().shape({
        name: yup.string().required('Поле обязательно').matches(/[A-Za-z ]/gi, 'Только латинские'),
        email: yup.string().email('Неверный email').required('Поле обязательно'),
        password: yup.string().required('Поле обязательно'),
        repeatPassword: yup.string().required('Поле обязательно')
    })

    return (
        <>
            <h1>Sign Up</h1>
            <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
                <Form>
                    <CustomInput
                        name='name'
                        type='text'
                        placeholder='Name'
                    />
                    <CustomInput
                        name='age'
                        type='text'
                        placeholder='Age'
                    />
                    <CustomInput
                        name='city'
                        type='text'
                        placeholder='City'
                    />
                    <CustomInput
                        name='email'
                        type='text'
                        placeholder='Email'
                    />
                    <CustomInput
                        name='password'
                        type='password'
                        placeholder='Password'
                    />
                    <CustomInput
                        name='repeatPassword'
                        type='password'
                        placeholder='Repeat password'
                    />

                    <button type={"submit"}>Submit</button>

                </Form>
            </Formik>
        </>
    )
}

export default SignUpPage;