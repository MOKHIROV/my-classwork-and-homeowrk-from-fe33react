import React, { useRef } from 'react';

const FormWithRef = () => {
    const nameRef = useRef(null);
    const emailRef = useRef(null);
    const passwordRef = useRef(null);

    const handleSubmit = (e) => {
        e.preventDefault();
        const name = nameRef.current.value;
        const email = emailRef.current.value;
        const password = passwordRef.current.value;

        console.log({ name, email, password })
    }

    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <h3>REF</h3>
            <input
                ref={nameRef}
                type="text"
                name="name"
                placeholder="Name"
            />

            <input
                ref={emailRef}
                type="text"
                name="email"
                placeholder="Email"
            />

            <input
                ref={passwordRef}
                type="password"
                name="password"
                placeholder="Password"
            />

            <button type="submit">Submit</button>
        </form>
    )
}

export default FormWithRef;