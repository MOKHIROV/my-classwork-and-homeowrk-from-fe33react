import React, { useState } from 'react';

const FormWithHooks = () => {
    const [values, setValues] = useState({
        name: '',
        email: '',
        password: '',
    })

    const [errors, setErrors] = useState({
        name: '',
        email: '',
        password: '',
    })

    const [touched, setTouched] = useState({
        name: false,
        email: false,
        password: false,
    })

    const isDirty =  errors.name || errors.email || errors.password;

    const handleSubmit = (e) => {
        e.preventDefault();

        if (isDirty) {
            return;
        }
        console.log(values);
    }

    const handleChange = (event) => {
        const { name, value } = event.target;
        setValues(state => ({ ...state, [name]: value}));
    }

    const handleBlur = (event) => {
        const { name } = event.target;
        setTouched(state => ({ ...state, [name]: true}));
    }


    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <h3>HOOKS</h3>
            <input
                value={values.name}
                onChange={(event) => {
                    handleChange(event);

                    const { value } = event.target;

                    if (value.length <= 3) {
                        setErrors(state => ({ ...state, name: 'Имя должно быть более 3 символов' }))
                    } else if (value.length > 12){
                        setErrors(state => ({ ...state, name: 'Имя должно быть меньше 12 символов' }))
                    }
                    else {
                        setErrors(state => ({ ...state, name: '' }))
                    }
                }}
                onBlur={handleBlur}
                type="text"
                name="name"
                placeholder="Name"
            />
            <p className="error">{touched.name && errors.name}</p>

            <input
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
                type="text"
                name="email"
                placeholder="Email"
            />
            <p className="error">{errors.email}</p>


            <input
                value={values.password}
                onChange={handleChange}
                onBlur={handleBlur}
                type="password"
                name="password"
                placeholder="Password"
            />
            <p className="error">{errors.password}</p>


            <button disabled={isDirty} type="submit">Submit</button>
        </form>
    )
}

export default FormWithHooks;