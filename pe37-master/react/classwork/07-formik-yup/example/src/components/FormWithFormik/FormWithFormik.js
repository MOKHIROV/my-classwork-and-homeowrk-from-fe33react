import React from 'react';
import {Formik, Form, Field, ErrorMessage} from "formik";
import CustomInput from "../CustomInput";
import * as yup from 'yup';
import CustomInputWithoutField from "../CustomInputWithoutField";

const onSubmit = (values) => {
    console.log(values)
}

const initialValues = {
    name: '',
    email: '',
    password: '',
    age: ''
}

const validationSchema = yup.object().shape({
    name: yup.string()
        .required('Поле обязательно')
        .min(3, 'Должен быть больше 3')
        .max(6, 'Должен быть меньше 6'),
    email: yup.string().email('Неправильный email'),
    age: yup.string().required('Поле обязательно')
})

const FormWithFormik = () => {

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {({values}) => {

                return (<Form>
                    <h3>FORMIK</h3>

                    <Field
                        type="text"
                        name="name"
                        placeholder="Name"
                        id="name"
                        label='Name'
                        component={CustomInput}
                    />

                    <Field
                        type="text"
                        name="email"
                        placeholder="Email"
                    />
                    <ErrorMessage name="email" render={msg => <p className="error">{msg}</p>}/>


                    <Field
                        type="password"
                        name="password"
                        placeholder="Password"
                    />

                    <CustomInputWithoutField
                        type="text"
                        placeholder="Your age"
                        name="age"
                    />

                    <button disabled={!(values.name || values.email || values.password)} type="submit">Submit</button>
                </Form>)
            }}

        </Formik>
    )
}

export default FormWithFormik;