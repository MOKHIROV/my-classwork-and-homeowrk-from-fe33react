import React from 'react';
import {useField} from "formik";

const CustomInputWithoutField = (props) => {
    const {placeholder} = props;

    const [field, meta, helpers] = useField(props);
    console.log(meta)


    return (
        <>
            <input
                // value={field.value}
                // onChange={field.onChange}
                // onBlur={field.onBlur}
                // name={field.name}
                type="text"
                placeholder={placeholder}
                {...field}
            />
            <p className="error">{meta.touched && meta.error}</p>
        </>
    )
}

export default CustomInputWithoutField;