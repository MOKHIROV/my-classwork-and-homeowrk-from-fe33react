import React from 'react';
import PropTypes from 'prop-types';

const CustomInput = (props) => {
    const { type, placeholder, id, label, form, field } = props;
    const { touched, errors } = form;

    return (
        <>
            <label htmlFor="">{label}</label>
            <input
                value={field.value}
                onChange={field.onChange}
                onBlur={field.onBlur}
                id={id}
                type={type}
                placeholder={placeholder}
            />
            <p className="error">{touched[field.name] && errors[field.name]}</p>
        </>
    );
}

CustomInput.propTypes = {
    type: PropTypes.string,
    placeholder: PropTypes.string,
};
CustomInput.defaultProps = {
    type: 'text',
    placeholder: '',
};

export default CustomInput;