import React, {useState} from 'react';

const FormWithHooks = () => {
    const [values, setValues] = useState({
        name: '',
        email: '',
        password: '',
    });

    const [errors, setErrors] = useState({
        name: '',
        email: '',
        password: '',
    });

    const [touched, setTouched] = useState({
        name: false,
        email: false,
        password: false,
    });

    const isDirty = !!errors.name || !!errors.email || !!errors.password

    const handleChange = (name, value) => {
        setValues(current => ({ ...current, [name]: value }))
    }

    const validateName = (value) => {
        if (/[^A-Za-z]/gi.test(value)) {
            setErrors(current => ({ ...current, name: 'Only latin letters' }))
        } else if (value.length > 24) {
            setErrors(current => ({ ...current, name: 'Name should be less then 24 symbols' }))
        } else {
            setErrors(current => ({ ...current, name: '' }))
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        console.log(values)

    }

    return (
        <form onSubmit={(e) => handleSubmit(e)}>
            <h3>HOOKS</h3>
            <input
                type="text"
                name="name"
                placeholder="Name"
                value={values.name}
                onChange={({ target }) => {
                    handleChange(target.name, target.value);
                    if (touched.name) {
                        validateName(target.value);
                    }
                }}
                onBlur={({ target }) => {
                    if (!touched.name) {
                        setTouched(current => ({ ...current, name: true }));
                        validateName(target.value);
                    }
                }}
            />
            <span className="error">{errors.name}</span>

            <input
                type="text"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={({ target }) => handleChange(target.name, target.value)}
            />
            <span className="error">{errors.email}</span>


            <input
                type="password"
                name="password"
                placeholder="Password"
                value={values.password}
                onChange={({ target }) => handleChange(target.name, target.value)}
            />
            <span className="error">{errors.password}</span>

            <button disabled={isDirty} type="submit" >Submit</button>
        </form>
    )
}

export default FormWithHooks;