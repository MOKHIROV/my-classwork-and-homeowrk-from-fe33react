import React from "react";
import {Switch, Route, Redirect} from 'react-router-dom'

import FormWithRef from "../components/FormWithRef";
import FormWithHooks from "../components/FormWithHooks";
import FormWithFormik from "../components/FormWithFormik";

const Routes = () => (
    <Switch>
        <Route exact path="/"><Redirect to='/ref'/></Route>
        <Route exact path="/ref">
            <FormWithRef />
        </Route>

        <Route exact path="/hooks">
            <FormWithHooks />
        </Route>

        <Route exact path="/formik">
            <FormWithFormik />
        </Route>
    </Switch>
);


export default Routes