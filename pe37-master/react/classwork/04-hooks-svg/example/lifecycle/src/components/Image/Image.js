import React, { useEffect } from "react";
import getNameWithTime from "../../utils/getNameWithTime";

const Image = ({ src, counter }) => {
    // const { src } = props;

    const logScroll = () => {
        console.log(Math.random(), window.scrollY);
    }

    useEffect(() => {
        getNameWithTime('Image','DID MOUNT');
        window.addEventListener('scroll', logScroll);

        return () => {
            window.removeEventListener('scroll', logScroll);
        }
    }, []);

    useEffect(() => {
        getNameWithTime('Image','DID UPDATE');

    }, [src, counter]);

    return (
        <img style={{ backgroundColor: 'lightgray' }} src={src} alt="Some alt" width={400} height={200} />
    )
}

// class Image extends PureComponent {
//
//     constructor() {
//         getNameWithTime('Image', 'CONSTRUCTOR');
//         super();
//     }
//
//     logScroll = () => {
//         console.log(Math.random(), window.scrollY);
//     }
//
//     componentDidMount() {
//         getNameWithTime('Image','DID MOUNT');
//         window.addEventListener('scroll', this.logScroll);
//     }
//
//     componentDidUpdate() {
//         getNameWithTime('Image','DID UPDATE');
//
//     }
//
//     componentWillUnmount() {
//         getNameWithTime('Image','WILL UNMOUNT');
//         window.removeEventListener('scroll', this.logScroll)
//     }
//
//
//     render(){
//         getNameWithTime('Image','RENDER');
//         const { src } = this.props;
//
//         return (
//             <img style={{ backgroundColor: 'lightgray' }} src={src} alt="Some alt" width={400} height={200} />
//         );
//     }
// }

export default Image;
