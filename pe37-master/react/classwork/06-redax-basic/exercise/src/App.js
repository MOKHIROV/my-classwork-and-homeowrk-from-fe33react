import './App.scss';
import AddNote from "./components/AddNote";
import NoteItem from "./components/NoteItem";
import NoteContainer from "./components/NoteContainer";
import {Provider} from "react-redux";
import store from "./store";
import Modal from "./components/Modal";

// const notes = [
//     {
//         id: 0,
//         text: 'JavaScript / React'
//     },
//     {
//         id: 1,
//         text: 'CSS / SASS (SCSS)'
//     },
//     {
//         id: 2,
//         text: 'HTML'
//     },
//     {
//         id: 3,
//         text: 'GIT'
//     }
// ]

function App() {
    return (
        <Provider store={store}>
            <div className="App">
                <AddNote/>
                <Modal />
                <NoteContainer/>
            </div>
        </Provider>
    );
}

export default App;
