import {
    ADD_NOTE,
    FETCH_DATA,
    SET_IS_LOADING,
    TOGGLE_ISDONE,
    CHANGE_TEXT,
    DELETE_NOTE,
    FETCH_DATA_FROM_LS
} from "../actions/notesAction";

const initialState = {
    notes: [],
    isLoading: false
};

const notesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DATA:
            const newState = {...state, notes: action.payload}

            localStorage.setItem('notes', JSON.stringify(newState));
            return newState

        case FETCH_DATA_FROM_LS: {
            return {...state, notes: action.payload}
        }

        case ADD_NOTE:{

            localStorage.setItem('notes', JSON.stringify({...state, notes: [...state.notes, action.payload]}));
            return {...state, notes: [...state.notes, action.payload]};
        }

        case TOGGLE_ISDONE: {
            const tempNotes = [...state.notes];

            const currIndex = tempNotes.findIndex((elem) => elem.id === action.payload)
            tempNotes[currIndex].isDone = !tempNotes[currIndex].isDone;

            localStorage.setItem('notes', JSON.stringify({...state, notes: tempNotes}));
            return {...state, notes: tempNotes}
        }

        case SET_IS_LOADING:

            localStorage.setItem('notes', JSON.stringify({...state, isLoading: action.payload}));
            return {...state, isLoading: action.payload}

        case CHANGE_TEXT:
            const tempNotes = [...state.notes];
            const currIndex = tempNotes.findIndex((elem) => elem.id === action.payload.id)
            tempNotes[currIndex].text = action.payload.text;

            localStorage.setItem('notes', JSON.stringify({...state, notes:tempNotes}));
            return {...state,notes:tempNotes}

        case DELETE_NOTE:

            localStorage.setItem('notes', JSON.stringify({...state, notes: action.payload}));
            return {...state, notes: action.payload}

        default:
            return state;

    }
};
export default notesReducer;