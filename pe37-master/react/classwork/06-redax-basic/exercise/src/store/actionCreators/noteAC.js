import {FETCH_DATA_FROM_LS, ADD_NOTE, TOGGLE_ISDONE, FETCH_DATA, SET_IS_LOADING, CHANGE_TEXT, DELETE_NOTE} from "../actions/notesAction";
import {setIsOpenModalAC} from "./modalAC";

// export const addNote = (value) => ({ type: ADD_NOTE, payload: value })
// export const toggleIsDone = (id) => ({type: TOGGLE_ISDONE, payload: id})
export const toggleIsDone = (id) => async (dispatch) => {
    const {data,status} = await fetch('http://localhost:3001/todo',{
        method:'PATCH',
        headers:{
            "content-type":"application/json"
        },
        body:JSON.stringify({id})
    })
        .then(rsp => rsp.json());
    if(status === "success"){
        dispatch({type: TOGGLE_ISDONE, payload: id})
    }
}
// export const addNote = (value) => (disptach) => {
//
// }
export const changeText = (id,text) =>async (dispatch)=>{
    const {status} = await fetch('http://localhost:3001/todo', {
        method: 'PUT',
        body: JSON.stringify({id,text}),
        headers: {
            "content-type": "application/json"
        }
    }).then(res => res.json())
    if(status === "success"){
        dispatch({type:CHANGE_TEXT,payload:{id,text}})
    }
}
export const addNote = (value) => {
    console.log('VALUE', value)
    return async (dispatch) => {

        const {data} = await fetch('http://localhost:3001/todo', {
            method: 'POST',
            body: JSON.stringify({text: value}),
            headers: {
                "content-type": "application/json"
            }
        }).then(res => res.json())


        dispatch({type: ADD_NOTE, payload: data})
    }
}
export const deleteNote = (id) => async (dispatch) => {
        const {data, status} = await fetch(`http://localhost:3001/todo?id=${id}`, {
            method: 'DELETE',
        }).then(res => res.json())
    console.log(data)
    if(status === 'success'){
        dispatch({type: DELETE_NOTE, payload: data})
        dispatch(setIsOpenModalAC(false))

    }

    }

export const fetchData = () => {

    return async (dispatch) => {
        dispatch(setIsLoading(true))
        const {data} = await fetch('http://localhost:3001/todo').then(res => res.json())

        dispatch(setIsLoading(false))
        dispatch({type: FETCH_DATA, payload: data})
    }
}
export const setIsLoading = (value) => ({type: SET_IS_LOADING, payload: value})

export const fetchDataFromLS = (data) => ({type: FETCH_DATA_FROM_LS, payload: data})