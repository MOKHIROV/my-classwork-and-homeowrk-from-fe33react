export const ADD_NOTE = "ADD_NOTE";
export const TOGGLE_ISDONE = "TOGGLE_ISDONE";
export const FETCH_DATA = "FETCH_DATA"
export const SET_IS_LOADING = "SET_IS_LOADING"
export const CHANGE_TEXT = "CHANGE_TEXT"
export const DELETE_NOTE = 'DELETE_NOTE'
export const FETCH_DATA_FROM_LS = 'FETCH_DATA_FROM_LS';


