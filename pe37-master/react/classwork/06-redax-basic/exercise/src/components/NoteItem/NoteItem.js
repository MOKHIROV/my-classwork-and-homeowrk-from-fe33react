import React, {useState} from 'react';
import classNames from "classnames";
import PropTypes from 'prop-types';
import styles from './NoteItem.module.scss';
import {useDispatch} from "react-redux";
import {Button, Checkbox, TextField} from "@mui/material";
import { ReactComponent as EditSVG } from "../../assets/edit.svg";
import { ReactComponent as DeleteSVG } from "../../assets/delete.svg";
import {changeText, toggleIsDone} from "../../store/actionCreators/noteAC";
import { ReactComponent as CheckSVG } from "../../assets/check.svg";
import {setConfig, setIsOpenModalAC} from "../../store/actionCreators/modalAC";

const NoteItem = (props) => {
    const [isEdit,setIsEdit] = useState(false)
    const [value, setValue] = useState("");
    const { index, text, id, isDone } = props;
    const dispatch = useDispatch()
    if (isEdit){
        return(<li className={styles.rootEdit}>
            <input value={value}
                   onChange={(e)=>{setValue(e.target.value)}} label='Edit Note' className={styles.input} />
            <Button variant='contained' color='success' className={styles.btn} onClick={()=>{
                dispatch(changeText(id,value))
                setIsEdit(false)}}><CheckSVG /></Button>
        </li>)
    }

    return (
        <li className={classNames(styles.root, { [styles.rootDone]: isDone })}>
            <div className={styles.wrapper}>

                <Checkbox
                    checked={isDone}
                    onChange={()=>{ dispatch(toggleIsDone(id))}}
                    className={styles.checkbox}
                />

                <span>{index}.</span>
                <p className={classNames({ [styles.done]: isDone })}>{text}</p>
            </div>

            <div className={styles.wrapper}>
                <Button onClick={()=> setIsEdit(true)} variant='contained' className={styles.btn}><EditSVG /></Button>
                <Button onClick={()=>{
                    dispatch(setIsOpenModalAC(true))
                    dispatch(setConfig(id, text))
                }} variant='contained' color='error' className={styles.btn}><DeleteSVG /></Button>
            </div>
        </li>
    )
}

NoteItem.propTypes = {
    index: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    text: PropTypes.string,
};
NoteItem.defaultProps = {
    text: '',
};

export default NoteItem;



