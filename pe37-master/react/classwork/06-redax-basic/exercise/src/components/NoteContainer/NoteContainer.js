import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import NoteItem from "../NoteItem";
import {useDispatch, useSelector} from "react-redux";
import {fetchDataFromLS} from "../../store/actionCreators/noteAC";
import {fetchData} from '../../store/actionCreators/noteAC'
import {FETCH_DATA} from "../../store/actions/notesAction";

const NoteContainer = () => {

    const notes = useSelector(({notes}) => notes.notes);
    const isLoading = useSelector(({notes}) => notes.isLoading );
    console.log(notes);

    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(fetchData())
    }, [])

    console.log('notes', notes)

    if (!notes) return <p style={{ fontSize: 24 }}>You don't have any notes yet</p>;
    if (isLoading) return <h1>Loading...</h1>

    return (
        <ul>
            {notes && notes.map(({ text, id, isDone }, index) => <NoteItem id={id} isDone={isDone} index={index + 1} text={text} key={id} />)}
        </ul>
    )
}

NoteContainer.propTypes = {
    notes: PropTypes.array,
};
NoteContainer.defaultProps = {
    notes: null,
};

export default NoteContainer;