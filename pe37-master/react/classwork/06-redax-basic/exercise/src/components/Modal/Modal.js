import React from 'react';
import styles from './Modal.module.scss';
import {Button} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {setIsOpenModalAC} from "../../store/actionCreators/modalAC";
import {deleteNote} from "../../store/actionCreators/noteAC";

const Modal = () => {
    const {isOpen, config} = useSelector(({modal})=>modal)

const dispatch  = useDispatch()
    if (!isOpen){
        return null;
    }
    console.log(config.id)
    return (
        <div className={styles.root}>
            <div className={styles.background} onClick={()=>{dispatch(setIsOpenModalAC(false))}}/>
            <div className={styles.content}>
                <div className={styles.closeWrapper}>
                    <Button onClick={()=>{dispatch(setIsOpenModalAC(false))}} variant="text" color="warning">Close</Button>
                </div>
                <h2>Do you really want to delete "{config.text}"</h2>
                <div className={styles.buttonContainer}>
                    <Button onClick={()=>{dispatch(deleteNote(config.id))}} variant="contained">YES</Button>
                    <Button onClick={()=>{dispatch(setIsOpenModalAC(false))}} variant="contained" color="warning">NO</Button>
                </div>
            </div>
        </div>
    );
};

export default Modal;
