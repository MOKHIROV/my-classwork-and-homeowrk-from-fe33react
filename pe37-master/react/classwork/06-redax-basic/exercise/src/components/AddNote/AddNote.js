import React, {useState} from 'react';
import styles from './AddNote.module.scss';
import {Button, TextField} from "@mui/material";
import {useDispatch} from "react-redux";
import {addNote} from "../../store/actionCreators/noteAC";

const AddNote = () => {
    const dispatch = useDispatch();
    const [value, setValue] = useState("");

    return (
        <>
            <div className={styles.root}>
                <input
                    type='text'
                    placeholder='Your note'
                    value={value}
                    onChange={(e)=>{setValue(e.target.value)}}
                    className={styles.input}
                />
                <Button onClick={() => {
                    dispatch(addNote(value));
                    setValue('');
                }} className={styles.btn} variant='contained'>Add Note</Button>
            </div>
            <div className={styles.line} />
        </>
)
}

export default AddNote;