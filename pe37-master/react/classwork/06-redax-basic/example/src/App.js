import './App.css';
import Header from "./components/Header";
import { BrowserRouter as Router } from "react-router-dom";
import Routes from "./Routes";
import {Provider} from "react-redux";
import store from "./store";

function App() {
  return (
      <Provider store={store}>
          <Router>
            <div className="App">
                <Header />
                <section>
                    <Routes />
                </section>
            </div>
          </Router>
      </Provider>
  );
}

export default App;
