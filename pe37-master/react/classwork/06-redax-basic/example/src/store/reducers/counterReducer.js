import {INCREMENT, CLEAR, SET, DECREMENT, NEW_TITLE, ADD_NEW_FACT} from "../actions/counterActions";

const initialState = {
    counter: 0,
    title: 'SOME TITLE',
    fact: '',
}

const counterReducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT: {
            return { ...state, counter: state.counter + 1 }
        }
        case DECREMENT: {
            if (state.counter > 0) {
                return { ...state, counter: state.counter - 1 }
            }
            return state;
        }

        case CLEAR: {
            return { ...state, counter: 0 }
        }

        case SET: {
            if (action.payload && typeof action.payload === 'number' ) {
                return { ...state, counter: action.payload }
            }
            return state;
        }

        case NEW_TITLE: {
            return { ...state, title: 'NEW TITLE' }
        }



        default: {
            return state;
        }
    }
}

export default counterReducer;