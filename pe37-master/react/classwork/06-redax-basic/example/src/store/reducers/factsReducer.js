import {ADD_NEW_FACT, SET_IS_LOADING} from '../actions/fatcsActions';

const initialState = {
    data: '',
    isLoading: false,
}

const factReducer = (state = initialState, action) => {
    switch (action.type) {

        case ADD_NEW_FACT: {
            return { ...state, data: action.payload }
        }

        case SET_IS_LOADING: {
            return { ...state, isLoading: action.payload }
        }

        default: {
            return state;
        }
    }
}

export default factReducer;