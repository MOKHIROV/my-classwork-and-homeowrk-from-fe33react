import {combineReducers} from "redux";
import counterReducer from "./counterReducer";
import factReducer from "./factsReducer";

const appReducer = combineReducers({
    counter: counterReducer,
    fact: factReducer
})

export default appReducer;