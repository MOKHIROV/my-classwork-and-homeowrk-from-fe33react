export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const CLEAR = 'CLEAR';
export const SET = 'SET';
export const NEW_TITLE = 'NEW_TITLE';
