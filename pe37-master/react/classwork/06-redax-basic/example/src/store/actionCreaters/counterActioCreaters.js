import {CLEAR, DECREMENT, INCREMENT, SET, NEW_TITLE, ADD_NEW_FACT} from "../actions/counterActions";

export const incrementCounterAC = () => ({ type: INCREMENT });
export const decrementCounterAC = () => ({ type: DECREMENT });
export const clearCounterAC = () => ({ type: CLEAR });
export const newTitleAC = () => ({ type: NEW_TITLE });
export const setCounterAC = (counterValue) => ({ type: SET, payload: counterValue });
