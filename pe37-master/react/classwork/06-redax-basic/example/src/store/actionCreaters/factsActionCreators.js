import {ADD_NEW_FACT, SET_IS_LOADING} from "../actions/fatcsActions";



export const setIsLoadingAC = (value) => ({ type: SET_IS_LOADING, payload: value });

export const getCatFactsAC = () => async (dispatch) => {
    dispatch(setIsLoadingAC(true));
    const data = await fetch('https://catfact.ninja/fact').then(res => res.json());

    dispatch(setIsLoadingAC(false));
    dispatch({ type: ADD_NEW_FACT, payload: data })
}