import React from 'react';
import Counter from "../../components/Counter";
import {useDispatch, useSelector} from "react-redux";
import {getCatFactsAC} from "../../store/actionCreaters/factsActionCreators";


const CounterPage = () => {
    const dispatch = useDispatch();
    const { data, isLoading } = useSelector(({ fact }) => fact)


    if (isLoading) {
        return (
            <>
                <h1>Loading ...</h1>
            </>
        )
    }

    return (
        <>
            <h1>Counter</h1>
            <p>{data.fact}</p>
            <button onClick={() => {
                dispatch(getCatFactsAC())
            }}>GET FACTS</button>

            <Counter/>
        </>
    )
};


export default CounterPage;