import React, {useState} from 'react';
import styles from './Counter.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {incrementCounterAC, decrementCounterAC, clearCounterAC, setCounterAC} from "../../store/actionCreaters/counterActioCreaters";

const Counter = () => {
    const dispatch = useDispatch();
    const counter = useSelector(({ counter }) => counter.counter);

    const [value, setValue] = useState('');

    const incrementCounter = () => {
        dispatch(incrementCounterAC());
    }

    const decrementCounter = () => {
        dispatch(decrementCounterAC());
    }

    const clearCounter = () => {
        dispatch(clearCounterAC());
    }

    const setCounter = () => {
        dispatch(setCounterAC(+value));
        setValue('');
    }

    return (
        <div className={styles.root}>
            <p className={styles.counter}>{counter}</p>
            <div className={styles.btnWrapper}>
                <button onClick={incrementCounter}>+</button>
                <button onClick={decrementCounter}>-</button>
                <button onClick={clearCounter}>Clear counter</button>
            </div>

            <div className={styles.btnWrapper}>
                <input value={value} onChange={({ target }) => setValue(target.value)} type="text" />
                <button onClick={setCounter}>Set Value</button>
            </div>
        </div>
    )
}

export default Counter;