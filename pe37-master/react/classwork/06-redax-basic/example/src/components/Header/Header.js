import React from 'react';
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import { newTitleAC } from "../../store/actionCreaters/counterActioCreaters";

const Header= () => {
    const title = useSelector(({ counter }) => counter.title);
    const dispatch = useDispatch();
    return (
              <header className={styles.root}>
                  <span>{ title }</span>
                  <nav>
                      <ul>
                          <li>
                              <NavLink exact activeClassName={styles.active} to="/">Home</NavLink>
                          </li>
                          <li>
                              <NavLink activeClassName={styles.active} to="/counter">Counter</NavLink>
                          </li>
                      </ul>
                  </nav>
                  <button onClick={() => {
                      dispatch(newTitleAC());
                  }}>CHANGE T</button>
              </header>
        );
};

export default Header;
