import React, { PureComponent } from 'react';
import styles from './Header.module.scss';
import PropTypes from 'prop-types';
import Select from "../Select/Select";
class Header extends PureComponent {
    render(){
        const { title, user: { name, age, avatar }, userId, changeUserId} = this.props;

        return (
              <header className={styles.root}>
                  <span>{title}</span>
                  <Select userId={userId} changeUserId={changeUserId} />
                  <div className={styles.userContainer}>
                      <img src={avatar} alt={`user ${name}`} />
                      <span>{name}, {age}</span>
                  </div>
              </header>
        );
    }


};
Header.propTypes = {
    title: PropTypes.string,
    user: PropTypes.shape({
        name: PropTypes.string,
        age : PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        avatar : PropTypes.string
    })

}
Header.defaultProps = {
    title : '',
    user: null,
}

export default Header;
