import React, { PureComponent } from 'react';
import Post from "../Post";
import styles from './PostsContainer.module.scss';
import PropTypes from 'prop-types';
import Preloader from "../Preloader";

class PostsContainer extends PureComponent {
    render(){
        const { posts, isLoading } = this.props;

        return (
              <section className={styles.root}>
                  <h1>POSTS</h1>
                      <div className={styles.postsContainer}>


                          {isLoading
                              ? <Preloader/>
                              : <>{posts.map(({id, ...args}) => <Post key={id} {...args} />)}</>
                          }



                      </div>
              </section>
        );
    }


}

PostsContainer.propTypes = {
    posts: PropTypes.array,
    isLoading: PropTypes.bool,
}

PostsContainer.defaultProps = {
    posts: [],
    isLoading: false,
}


export default PostsContainer;
