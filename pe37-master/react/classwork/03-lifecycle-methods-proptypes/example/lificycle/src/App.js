import { Component } from 'react';
import getNameWithTime from "./utils/getNameWithTime";
import Image from "./components/Image";
import dogImage from './assets/1.jpg';

class App extends Component {

    constructor() {
        getNameWithTime('App', 'CONSTRUCTOR');
        super();

        this.state = {
            counter: 0,
            src: null,
        }
    }

    async componentDidMount() {
        const data = localStorage.getItem('data');

        if (data) {
            console.log(data)
        } else {
            const result = await fetch('./data/test.json').then(res => res.json());
            console.log(result);
        }

        // const result = await fetch('./data/test.json').then(res => res.json());
        //
        // setTimeout(() => {
        //     localStorage.setItem('data', JSON.stringify({name: 'Sam', age: '22'}));
        // }, 5000)

        // console.log(result)
        getNameWithTime('App','DID MOUNT');
    }

    componentDidUpdate() {
        getNameWithTime('App','DID UPDATE');
    }

    componentDidCatch(error, errorInfo) {
        console.log('COMPONENT DID CATCH')
        console.log(error);
        console.log(errorInfo);
    }

    static getDerivedStateFromError(error) {
        console.log('GET DERIVED STATE FROM ERROR');
        console.log(error);
    }

    render(){
        console.log('dogImage', dogImage)
        const { counter, src } = this.state;
          getNameWithTime('App','RENDER');
          return (
            <div className="App">
                <h1>
                    <a href="https://ru.reactjs.org/docs/react-component.html" target="_blank" rel="noreferrer">
                        Состояние и жизненный цикл
                    </a>
                </h1>


                {/*<img src="./images/1.jpg" alt="fdg"/>*/}
                <img src={dogImage} alt="fdg"/>

                <p>{ counter }</p>
                <button onClick={() => this.setState(current => ({...current, counter: current.counter + 1}))}>Increment counter</button>

                <div>
                    <button onClick={() => this.setState({ src: `https://picsum.photos/400/200?$versio=${Math.random()}` })}>Generate image src</button>
                    <button onClick={() => this.setState({ src: null })}>Delete image src</button>
                </div>

                {src && <Image src={src} />}
                </div>
        );
    }
}

export default App;
