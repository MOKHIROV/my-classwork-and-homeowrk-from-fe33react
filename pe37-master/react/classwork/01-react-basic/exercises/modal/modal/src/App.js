import React from "react";
import './App.scss'

class App extends React.Component {

    state = {
        isOpenFirst: false,
        isOpenSecond: false,
    }

    openModal = (isSecond = false) => {
        if (isSecond) {
            this.setState({isOpenSecond: true})
        } else {
            this.setState({isOpenFirst: true})
        }
    }

    closeModal = (isSecond = false) => {
        if (isSecond) {
            this.setState({isOpenSecond: false})
        } else {
            this.setState({isOpenFirst: false})
        }
    }

    render() {
        const {isOpenFirst, isOpenSecond} = this.state;

        return (
            <>
                {isOpenFirst && <div className="modal">
                    <div onClick={() => this.closeModal()} className="background"/>
                    <div className="content">
                        <button className="close-btn" onClick={() => this.closeModal()}>X</button>
                        <h1>MODAL 1</h1>

                        <div className="button-container">
                            <button className="yes">YES</button>
                            <button className="no" onClick={() => this.closeModal()}>NO</button>
                        </div>
                    </div>
                </div>}

                {isOpenSecond && <div className="modal">
                    <div onClick={() => this.closeModal(true)} className="background"/>
                    <div className="content">
                        <button className="close-btn" onClick={() => this.closeModal(true)}>X</button>
                        <h1>MODAL 2</h1>

                        <div className="button-container">
                            <button className="yes">YES</button>
                            <button className="no" onClick={() => this.closeModal(true)}>NO</button>
                        </div>
                    </div>
                </div>}

                <button onClick={() => this.openModal()}>OPEN MODAL 1</button>
                <button onClick={() => this.openModal(true)}>OPEN MODAL 2</button>
            </>

        );
    }
}


export default App;