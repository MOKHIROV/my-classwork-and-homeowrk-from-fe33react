import { React, Component } from 'react';
import RenderCards from './components/renderCards/renderCards';
class App extends Component {
  state = {
    items: [],
  };

  async componentDidMount() {
    let fetchCards = await fetch('/data/sellCards.json')
      .then((res) => res.json())
      .then((data) => this.setState({ items: data }));
  }
  render() {
    return (
      <>
        {this.state.items.map((item, index) => {
          return (
            <RenderCards
              key={index}
              devicePhoto={item.devicePhoto}
              name={item.name}
              price={item.price}
              article={item.article}
              colour={item.colour}></RenderCards>
          );
        })}
      </>
    );
  }
}
export default App;
