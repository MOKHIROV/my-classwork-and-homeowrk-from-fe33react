import { React, Component } from 'react';
import style from '../renderCards/renderCards.module.scss';
import Modal from '../Modal/Modal';
import PropTypes from 'prop-types';
class RenderCards extends Component {
  render() {
    const { devicePhoto, name, article, colour, price } = this.props;
    return (
      <>
        <div className={style.cardsStyle}>
          <img className={style.images} src={devicePhoto}></img>
          <h1 className={style.firstH1}>{name}</h1>
          <p className={style.secondP}>Артикль: {article}</p>
          <p className={style.secondP}>Цвет: {colour}</p>
          <p className={style.priceP}>Цена: {price}</p>
          <Modal
            text="Do you want to"
            header="Are you sure you want to add this item to a cart?"></Modal>
        </div>
      </>
    );
  }
}

RenderCards.propTypes = {
  device: PropTypes.string,
  name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  article: PropTypes.string,
  colour: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.symbol,
  ]),
  price: PropTypes.string,
};
RenderCards.defaultProps = {
  device: '',
  name: 'name',
  article: null,
  colour: '#fff',
  price: null,
};
export default RenderCards;
