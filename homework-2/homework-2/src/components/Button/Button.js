import React, { useEffect, Component } from 'react';
import style from '../Button/Button.module.scss';
import SvgIcon from '../svg/SvgIcon';
import PropTypes from 'prop-types';
import SecondSvgIcon from '../svg/SecondSvgIcon';
class Button extends Component {
  state = {
    isFirst: true,
    isSecond: false,
  };
  changeColor = () => {
    this.setState({ isSecond: true });
    this.setState({ isFirst: false });
  };
  changeScColor = () => {
    this.setState({ isSecond: false, isFirst: true });
  };
  render() {
    const { backgroundcolor, handleClick, children } = this.props;
    const { isSecond, isFirst } = this.state;
    return (
      <>
        <div className={style.buttonDiv}>
          <button
            className={style.buttons}
            style={{ backgroundColor: backgroundcolor }}
            onClick={handleClick}>
            {children}
          </button>
          {isFirst && (
            <SvgIcon
              handleClick={() => {
                console.log(this.state);
                this.changeColor();
              }}
              className={style.svg}
            />
          )}
          {isSecond && (
            <SecondSvgIcon
              handleClick={() => {
                this.changeScColor();
              }}></SecondSvgIcon>
          )}
        </div>
      </>
    );
  }
}
Button.propTypes = {
  backgroundColor: PropTypes.string,
  handleClick: PropTypes.func,
  children: PropTypes.PropTypes.any.isRequired,
};
Button.defaultProps = {
  backgroundColor: 'grey',
  handleClick: () => {},
  children: '',
};
export default Button;
