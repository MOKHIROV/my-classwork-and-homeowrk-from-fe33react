import style from './Modal.module.scss';
import React, { Component } from 'react';
import Button from '../Button/Button';
import PropTypes from 'prop-types';
export default class Modal extends Component {
  state = {
    isOpned: false,
  };
  openModal = (isSecond = false) => {
    if (isSecond) {
      this.setState({ isOpned: true });
    }
  };

  closeModal = (isSecond = false) => {
    if (isSecond) {
      this.setState({ isOpned: false });
    }
  };

  render() {
    const { header, text } = this.props;
    const { isOpned } = this.state;
    return (
      <>
        <Button handleClick={() => this.openModal(true)} backgroundcolor="blue">
          Add to Cart
        </Button>
        {isOpned && (
          <div className={style.basicModal} text={text} header={header}>
            <div className={style.modal}>
              <p className={style.modalX} onClick={() => this.closeModal(true)}>
                X
              </p>
              <p className={style.modalTitle}>{header}</p>
              <p className={style.modalSecondText}>
                Press "Yeah" if you want add this item to your cart
              </p>
              <button
                className={style.modalFirstButton}
                onClick={() => this.closeModal(true)}>
                Yeah
              </button>
              <button
                className={style.modalSecondButton}
                onClick={() => this.closeModal(true)}>
                No
              </button>
            </div>
          </div>
        )}
      </>
    );
  }
}
Modal.propTypes = {
  header: PropTypes.string,
  text: PropTypes.string,
};
Modal.defaultProps = {
  header: 'Add to your cart?',
  text: 'Yes or No',
};
