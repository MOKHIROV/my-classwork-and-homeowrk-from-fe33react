import React from 'react';
import style from '../svg/SvgIcon.scss';
class SecondSvgIcon extends React.Component {
  render() {
    const { handleClick } = this.props;
    return (
      <>
        <p onClick={handleClick}>
          <svg className={style.svg} viewBox="0 0 24 24" fill="currentColor">
            <path d="M13.3459 20.1341C12.5822 20.824 11.4197 20.8218 10.6587 20.129L10.55 20.03C5.4 15.36 2 12.28 2 8.5C2 5.42 4.42 3 7.5 3C9.24 3 10.91 3.81 12 5.09C13.09 3.81 14.76 3 16.5 3C19.58 3 22 5.42 22 8.5C22 12.28 18.6 15.36 13.45 20.04L13.3459 20.1341Z"></path>
          </svg>
        </p>
      </>
    );
  }
}
export default SecondSvgIcon;
